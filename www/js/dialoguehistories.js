
var DialogueHistory = function(){

    //property
    var myCalendar;
    var myLetter,
        spouseLetter,
        mySelectedDate,
        spouseSelectedDate;
    var isLoading = false;
    // method
    this.init = init;
    this.loadPage = loadPage;

    function init(){    

    	myCalendar = null;
        myLetter = $$('.dialogues-page .card.mine .card-header.selected-question');
        spouseLetter = $$('.dialogues-page .card.spouse .card-header.selected-question');
        
        mySelectedDate = $$('.dialogues-page .card.mine .card-header span.selected-date');
        spouseSelectedDate = $$('.dialogues-page .card.spouse .card-header span.selected-date');
    	
        initEventListeners();
    }

    function loadPage(){
        mainView.router.load({pageName: 'dialogues'});
    }

    function initEventListeners(){
		myCalendar = myApp.calendar({
		    input: '#calendar-history',
		    dateFormat: 'DD, MM dd, yyyy',
		    disabled: {
		      from: new Date()
		    },
            firstDay: 0,
		    onChange: function(p, value){
                var d = new Date(value);
                getDialogueByDate(d);
            }
		});

        $$(document).on('click', '.dialogues-page .btn-calendar', function(){
        	myCalendar.open();
        });

        myLetter.html('No dialogues');
        spouseLetter.html('No dialogues');
    }

    myApp.onPageBeforeInit('dialogues', function(page){
        var today = new Date();
        getDialogueByDate(today);
    });

    function getDialogueByDate(d){
        var strday = d.getFullYear() + '-' + staticMgr.changeNumberFormat(d.getMonth() + 1) + '-' + staticMgr.changeNumberFormat(d.getDate()); 
        mySelectedDate.html(strday);
        spouseSelectedDate.html(strday);
        myLetter.html('No dialogues');
        spouseLetter.html('No dialogues');
        dbMgr.sendRequest('get_two_letters', {regdate: strday, history: '1'}, function(result){
            if (result['success']){
                var data = result['data'];
                for( var i = 0; i < data.length; i++){
                    var letter = data[i];
                    if ( letter['userid'] != undefined && letter['userid'] != null ) {
                        if ( letter['userid'] == user.userid ){
                            myLetter.html(letter['question'] + "<br>" + "I feel " + letter['feelings'] +" about this question." + "<br>" + letter['letter']);
                        }
                        else{
                            spouseLetter.html(letter['question'] + "<br>" + "I feel " + letter['feelings'] +" about this question." + "<br>" + letter['letter']);
                        }
                    }  
                }
            }
        });
    }
};

var historyMgr = new DialogueHistory();
historyMgr.init();