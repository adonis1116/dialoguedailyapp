
// Database Manage
var DBMgr = function(){

    // End Points
    var 
        // User
        URL_LOGIN = "user/login",
        URL_SIGNUP = "user/signup",
        URL_UPDATE_CONTACT_INFO = "user/updatecontactinfo",
        URL_UPGRADE_ACCOUNT = "user/upgradeaccount",
        URL_RECOVER_PASSWORD = "user/recoverpassword",
        URL_RESET_PASSWORD = "user/resetpassword",
        // Questions
        URL_GET_QUESTIONS = "question/questions",
        URL_GET_QUESTION_CATEGORIES = "question/categories",
        URL_GET_QUESTION_BY_ID = 'question/getbyid',

        // Feelings
        URL_GET_FEELINGS = "feeling/feelings",
        URL_GET_FEELING_CATEGORIES = "feeling/categories",
    
        // Timer
        URL_UPDATE_TIMER_DEFAULTS = "timer/updatetimerdefaults",
        URL_GET_TIMER_DEFAULTS = "timer/gettimerdefaults",

        // Appointment
        URL_GET_APPOINTMENT_DEFAULTS = "appointment/getdefaults",
        URL_UPDATE_APPOINTMENT_DEFAULTS = "appointment/updatedefaults",

        URL_GET_NEXT_APPOINTMENT = "appointment/getnextappointment",
        URL_CREATE_NEXT_APPOINTMENT = "appointment/createnextappointment",
        URL_CLEAR_NEXT_APPOINTMENT = "appointment/clear",
        URL_UPDATE_WRITING_STATUS = "appointment/updatestatus",

        // Email Wording
        URL_GET_EMAIL_WORDINGS = "wordings/getwords",
        URL_UPDATE_EMAIL_WORDINGS = "wordings/updatewords",

        // Letters
        URL_ADD_LETTER = "mailing/addLetter",
        URL_SHARE_LETTER = "mailing/shareLetter",
        URL_GET_LETTER = "mailing/getletter",
        URL_GET_TWO_LETTERS = "mailing/gettwoletters",

        // Resources
        URL_VIDEOS = "resources/videos",

        URL_FAKE = "";

    //method
    this.init = init;
    this.sendRequest = sendRequest;

    function init(){

    }

    function sendRequest(action, params, callback){

        var endpoint = options.api_url;
        var requestMethod = 'GET';
        switch(action){
            case 'login':
                endpoint += URL_LOGIN;
            break;
            case 'signup':
                endpoint += URL_SIGNUP;
            break;
            case 'upgrade_account':
                endpoint += URL_UPGRADE_ACCOUNT;
            break;
            case 'recover':
                endpoint += URL_RECOVER_PASSWORD;
            break;
            case 'resetpassword':
                endpoint += URL_RESET_PASSWORD;
            break;
            case 'questions':
                endpoint += URL_GET_QUESTIONS;
            break;         
            case 'question_categories':
                endpoint += URL_GET_QUESTION_CATEGORIES;
            break;
            case 'get_question_by_id':
                endpoint += URL_GET_QUESTION_BY_ID;
            break;
            case 'feeling':
                endpoint += URL_GET_FEELINGS;
            break;
            case 'feeling_categories':
                endpoint += URL_GET_FEELING_CATEGORIES;
            break;         
            case 'update_contact':
                endpoint += URL_UPDATE_CONTACT_INFO;
            break;
            case 'update_timer':
                endpoint += URL_UPDATE_TIMER_DEFAULTS;
            break;
            case 'get_timer':
                endpoint += URL_GET_TIMER_DEFAULTS;
            break;
            case 'get_appointment_defaults':
                endpoint += URL_GET_APPOINTMENT_DEFAULTS;
            break;
            case 'update_appointment_defaults':
                endpoint += URL_UPDATE_APPOINTMENT_DEFAULTS;
            break;
            case 'get_email_wordings':
                endpoint += URL_GET_EMAIL_WORDINGS;
            break;
            case 'update_email_wordings':
                endpoint += URL_UPDATE_EMAIL_WORDINGS;
            break;
            case 'get_next_appointment':
                endpoint += URL_GET_NEXT_APPOINTMENT;
            break;
            case 'create_next_appointment':
                endpoint += URL_CREATE_NEXT_APPOINTMENT;
            break;
            case 'clear_next_appointment':
                endpoint += URL_CLEAR_NEXT_APPOINTMENT;
            break;
            case 'update_appointment':
                endpoint += URL_UPDATE_WRITING_STATUS;
            break;
            case 'add_letter':
                endpoint += URL_ADD_LETTER;
            break;
            case 'share_letter':
                endpoint += URL_SHARE_LETTER;
            break;
            case 'get_letter':
                endpoint += URL_GET_LETTER;
            break;
            case 'get_two_letters':
                endpoint += URL_GET_TWO_LETTERS;
            break;
            case 'get_videos':
                endpoint += URL_VIDEOS;
            break;
        }
        if (action != 'signup' && action != 'recover') params['token'] = user.userInfo['token'];
        if (action != 'login' && action != 'signup' && action != 'recover') params['userid'] = user.userid;
        console.log(params);
        myApp.showIndicator();
        $$.ajax({
            url: endpoint,
            method: requestMethod,
            contentType: 'application/json',
            dataType: 'json',
            data: params,
            success: function(res) {
                myApp.hideIndicator();
                callback(res['data']);
            },
            error: function(res) {
                myApp.hideIndicator();
                myApp.alert("Check your network connection status", "Dialogue Daily");
            }
        });
    }
};