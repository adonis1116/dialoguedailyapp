var WriteLetter = function(){

    //property
    var currentStep = 1;
    var currentMood = 1;
    var MAX_FEELING_COUNT = 3;
    var selectedCount = 0;
    var selectedFeelings = "";
    var paused = true;
    var elapsedTime = 0;
    var totalTime = 0;
    var writingTimer;
    var letter = "";
    var alarmMedia;
    // method
    this.init = init;
    this.loadPage = loadPage;
    this.resumeWriting = resumeWriting;
    this.continueWriting = continueWriting;
    function init(){
        currentStep = 1;
        currentMood = 1;
        initEventListeners();
    }

    function initEventListeners(){
        $$(document).on('click', '.view-question-page .btn-negative', function(){
            currentMood = -1;
            loadFeelingsList();
            $$('.view-question-page .center.sliding').html('Choose Feelings (Max 3)');
        });        

        $$(document).on('click', '.view-question-page .btn-positive', function(){
            currentMood = 1;
            loadFeelingsList();
            $$('.view-question-page .center.sliding').html('Choose Feelings (Max 3)');
        });

        $$(document).on('click', '.view-question-page .feelings-list ul li.accordion-item .item-link', function(e){
            var categoryId = parseInt($$(this).data('id'));
            var childListBlock = ($$(this).parent()).find('.child-list');
            if ( childListBlock.find('li').length != 0 ) return;
            dbMgr.sendRequest('feeling', {category_id: categoryId}, function(result){
                var feelings = result['data'];
                for(var i = 0; i < parseInt(result['count']); i++){
                  childListBlock.append(
                        '<li>'+
                          '<label class="label-checkbox item-content">'+
                            '<input type="checkbox" name="my-checkbox" value="' + feelings[i]['content'] + '">'+
                            '<div class="item-media">'+
                              '<i class="icon icon-form-checkbox"></i>'+
                            '</div>'+
                            '<div class="item-inner">'+
                              '<div class="item-title">' + feelings[i]['content'] + '</div>'+
                            '</div>'+
                          '</label>'+
                        '</li>');
                }
            });    
        });

        $$(document).on('click', '.view-question-page .feelings-list ul li.accordion-item .child-list li', function(){
            var strFeeling = $$(this).find('input').val();
            if ($$(this).find('input').prop('checked')){
                if (selectedFeelings.indexOf(strFeeling) != -1){
                    selectedFeelings.replace(strFeeling, "");
                    selectedCount --;
                } 
            }
            else{
                selectedCount ++;
                var words = selectedFeelings.split(" ");
                if (selectedCount > MAX_FEELING_COUNT){
                    myApp.alert("You can choose only 3 feelings. Already selected.", "Dialogue Daily");
                    selectedCount --;
                    return;
                }
                else{
                    if (selectedFeelings.indexOf(strFeeling) == -1){
                        selectedFeelings+= (strFeeling + " ");
                    }
                }
            }
        });

        $$(document).on('click', '.view-question-page .btn-prev-step', function(){
            currentStep = 1;
            selectedFeelings = "";
            selectedCount = 0;
            $$('.view-question-page .step-buttons').css('visibility', 'hidden');
            showSteps(currentStep);
            $$('.view-question-page .center.sliding').html('Choose Emotion Category');
        });        

        $$(document).on('click', '.view-question-page .btn-next-step', function(){
            if (selectedCount == 0){
                myApp.alert("You should select at least 1 feeling.", "Dialogue Daily");
                return;
            }
            currentStep = 3;
            $$('.view-question-page .btn-timer').css('visibility', 'visible');
            $$('.view-question-page .write-question-buttons').css('display', 'flex');
            $$('.view-question-page .step-buttons').css('display', 'none');
            $$('.view-question-page .write-question-buttons').css('visibility', 'visible');
            $$('.view-question-page .btn-reference').css('visibility', 'visible');
            $$('.view-question-page .btn-write').css('visibility', 'visible');
            $$('.view-question-page .btn-reset').css('visibility', 'visible');
            $$('.view-question-page .btn-backup').css('visibility', 'visible');
            showSteps(currentStep);
            $$('.view-question-page .center.sliding').html('Write Dialogue Letter');
            // Should modify the timer value
            writingTimer = window.setInterval(function(){
                processTime();
            }, 1000);
        });
       
        $$(document).on('click', '.view-question-page .btn-write', function(){
            paused = !paused;
            if (paused){
                $$('.view-question-page .btn-write').addClass('color-blue');
                $$('.view-question-page .btn-write').removeClass('color-red');
                $$('.view-question-page .btn-write').html('Write');
                $$('.view-question-page #view-question-step3 #lettertext').prop('disabled', true);
            }
            else{
                $$('.view-question-page .btn-write').removeClass('color-blue');
                $$('.view-question-page .btn-write').addClass('color-red');
                $$('.view-question-page .btn-write').html('Pause');
                $$('.view-question-page #view-question-step3 #lettertext').prop('disabled', false);
            }
        });

        $$('.view-question-page #view-question-step3 .card.writing-card').on('click', function(){
            paused = false;
            $$('.view-question-page .btn-write').removeClass('color-blue');
            $$('.view-question-page .btn-write').addClass('color-red');
            $$('.view-question-page .btn-write').html('Pause');
            $$('.view-question-page #view-question-step3 #lettertext').prop('disabled', false);
        });

        $$('.view-question-page .btn-reference').on('click', function(){
            $$('.dialogue-helper-page .btn-home').css('visibility', 'hidden');
            gotoReferencePage();
        });

        $$('.view-question-page .btn-reset').on('click', function(){
            paused = true;
            elapsedTime = 0;
            var lefttime =  totalTime - elapsedTime;
            $$('.view-question-page span#lefttime').html("Timer : " + staticMgr.changeNumberFormat(parseInt(lefttime / 60)) + ":00");
            $$('.view-question-page textarea#lettertext').val("");
            $$('.view-question-page .btn-write').html('Write');
            $$('.view-question-page .btn-write').addClass('color-blue');
            $$('.view-question-page .btn-write').removeClass('color-red');
        }); 

        $$('.view-question-page .btn-backup').on('click', function(){
            var tempWriting = {
                totalTime: totalTime,
                elapsedTime: elapsedTime,
                questionid: user.nextAppointment['questionid'],
                question: user.todayQuestion,
                feelings: selectedFeelings,
                mood: currentMood,
                letter: $$('.view-question-page textarea#lettertext').val(),
                date: new Date()
            };
            user.backupWriting(tempWriting);
            window.clearInterval(writingTimer);
            $$('.view-question-page .btn-write').html('Write');
            $$('.view-question-page .btn-write').addClass('color-blue');
            $$('.view-question-page .btn-write').removeClass('color-red');
            $$('.view-question-page .btn-timer').css('visibility', 'hidden');
            mainView.router.load({pageName:'home'});
        });

        $$(document).on('click', '.view-question-page .btn-done', function(){
            if (($$('.view-question-page textarea#lettertext').val()).length == 0){
                myApp.alert("Please write your letter." , "Dialogue Daily");
                return;
            }
            doneWriting();
        }); 
    }

    function initDOMElements(){
        $$('.view-question-page .step-buttons').css('visibility', 'hidden');
        $$('.view-question-page .write-question-buttons').css('visibility', 'hidden');
        $$('.view-question-page .btn-reference').css('visibility', 'hidden');
        $$('.view-question-page .btn-timer').css('visibility', 'hidden');
        var feelingsList = staticMgr.screenHeight - staticMgr.navbarHeight - 44 - 32 * 2 - 100;
        $$('.view-question-page .feelings-list').css('height', feelingsList + 'px');
    }

    function doneWriting(){
        myApp.confirm('Send the Dialogue Letter and notify your spouse you are done writing?', 'Send Letter', function () {
            window.clearInterval(writingTimer);
            letter = $$('.view-question-page textarea#lettertext').val();
            var letterInfo = {
                questionid: user.nextAppointment['questionid'],
                feelings: selectedFeelings,
                letter_content: letter,
                date: user.nextAppointment['date']
            };
            dbMgr.sendRequest('add_letter', letterInfo, function(result){
                user.letterInfo = JSON.parse(JSON.stringify(letterInfo));
                myApp.alert(result['msg'], staticMgr.appString());
                homeMgr.loadPage();
            });
        });
    }

    function processTime(){
        if ( paused ) return;
        
        elapsedTime ++;

        var lefttime =  totalTime - elapsedTime;
        checkAlarmTime(lefttime);
        var strLeftTime = staticMgr.changeNumberFormat(parseInt(lefttime / 60)) + ":" + staticMgr.changeNumberFormat(elapsedTime % 60 == 0 ? 0 : 60 - (elapsedTime % 60));
        $$('.view-question-page span#lefttime').html("Timer : " + strLeftTime);
        if (lefttime <= 0) {
            window.clearInterval(writingTimer);
            $$('.view-question-page .btn-write').css('visibility', 'hidden');
            $$('.view-question-page .btn-reset').css('visibility', 'hidden');
            $$('.view-question-page .btn-backup').css('visibility', 'hidden');
            $$('.view-question-page #view-question-step3 #lettertext').prop('disabled', true);
            myApp.alert("Time is up. Please send this letter to your spouse.", "Dialogue Daily");
            doneWriting();
            return;
        }
    }

    myApp.onPageBack('view-question', function(page){
        window.clearInterval(writingTimer);
        $$('.view-question-page .btn-write').html('Write');
        $$('.view-question-page .btn-write').addClass('color-blue');
        $$('.view-question-page .btn-write').removeClass('color-red');
        $$('.view-question-page .btn-timer').css('visibility', 'hidden');
        $$('.view-question-page .step-buttons').css('display', 'flex');
    });

    function checkAlarmTime(lefttime){
        var alarm1Time, alarm2Time, alarm3Time;
        if (user.timerInfo['alarm1_unit'] == 'm') alarm1Time = parseInt(user.timerInfo['alarm1_time']) * 60;
        else alarm1Time = parseInt(user.timerInfo['alarm1_time']);
        if (user.timerInfo['alarm2_unit'] == 'm') alarm2Time = parseInt(user.timerInfo['alarm2_time']) * 60;
        else alarm2Time = parseInt(user.timerInfo['alarm2_time']);
        if (user.timerInfo['alarm3_unit'] == 'm') alarm3Time = parseInt(user.timerInfo['alarm3_time']) * 60;
        else alarm3Time = parseInt(user.timerInfo['alarm3_time']);

        if (alarm1Time == lefttime){
            if ( parseInt(user.timerInfo['alarm1_active']) == 1 ){
                playRing(user.timerInfo['alarm1_ring']);
            }
        } 
        else if (alarm2Time == lefttime){
            if ( parseInt(user.timerInfo['alarm2_active']) == 1 ){
                playRing(user.timerInfo['alarm2_ring']);
            }
        }
        else if (alarm3Time == lefttime){
            if ( parseInt(user.timerInfo['alarm3_active']) == 1 ){
                playRing(user.timerInfo['alarm3_ring']);
            }
        }
    }

    function playRing(ringSrc){
        var strPlatform = device.platform;
        var src = ringSrc;
        if ( src == null || src == undefined || src == "" || src == "" )
        if (strPlatform.toLowerCase() == "ios"){
            src = "audio/defaultalarm.mp3";
        }
        else if(strPlatform.toLowerCase() == "android"){
            src = "/android_asset/www/audio/defaultalarm.mp3";
        }

        alarmMedia = new Media(src, onMediaSuccess, onMediaError);
        alarmMedia.play({
            numberOfLoops: 1,
            playAudioWhenScreenIsLocked : false
        });
    }

    function onMediaSuccess(){
        alarmMedia = null;
    }

    function onMediaError(e){
        if (e.code == MediaError.MEDIA_ERR_ABORTED ){
            
            var strPlatform = device.platform;
            var src = "";
            if ( src == null || src == undefined || src == "" || src == "" )
            if (strPlatform.toLowerCase() == "ios"){
                src = "audio/defaultalarm.mp3";
            }
            else if(strPlatform.toLowerCase() == "android"){
                src = "/android_asset/www/audio/defaultalarm.mp3";
            }

            alarmMedia = new Media(src);
            alarmMedia.play({
                numberOfLoops: 1,
                playAudioWhenScreenIsLocked : false
            });
        }
    }

    function loadPage(){
        initDOMElements();
        currentStep = 1;
        selectedFeelings = "";
        selectedCount = 0;
        paused = true;
        elapsedTime = 0;
        totalTime = parseInt(user.timerInfo['main_timer']) * 60;
        letter = "";
        $$('.view-question-page #view-question-step3 #lettertext').val("");
        $$('.view-question-page #view-question-step3 #lettertext').prop('disabled', true);
        showSteps(currentStep);
        $$('.view-question-page #view-question-step1 span#todayquestion').html(user.todayQuestion);
        $$('.view-question-page #view-question-step3 span#todayquestion').html(user.todayQuestion);
        
        $$('.view-question-page .btn-timer').css('visibility', 'hidden');
        $$('.view-question-page span#lefttime').html("Timer : " + staticMgr.changeNumberFormat(parseInt(totalTime / 60)) + ":00");
        $$('.view-question-page .btn-write').html('Write');
        $$('.view-question-page .btn-write').addClass('color-blue');
        $$('.view-question-page .btn-write').removeClass('color-red');
        $$('.view-question-page .btn-write').css('visibility', 'hidden');
        $$('.view-question-page .btn-reset').css('visibility', 'hidden');
        $$('.view-question-page .btn-backup').css('visibility', 'hidden');
        mainView.router.load({pageName: 'view-question'});
    }

    function continueWriting(content){
        var writingContents = JSON.parse(JSON.stringify(content));
        loadPage();
        currentMood = parseInt(writingContents['mood']);
        loadFeelingsList();
        currentStep = 2;
        showSteps(currentStep);
        currentStep = 3;
        $$('.view-question-page .write-question-buttons').css('display', 'flex');
        $$('.view-question-page .step-buttons').css('display', 'none');
        $$('.view-question-page .write-question-buttons').css('visibility', 'visible');
        $$('.view-question-page .btn-reference').css('visibility', 'visible');
        $$('.view-question-page .btn-write').css('visibility', 'visible');
        $$('.view-question-page .btn-reset').css('visibility', 'visible');
        $$('.view-question-page .btn-backup').css('visibility', 'visible');
        showSteps(currentStep);
        
        selectedFeelings = writingContents['feelings'];
        paused = true;
        elapsedTime = writingContents['elapsedTime'];
        totalTime = parseInt(writingContents['totalTime']);
        letter = writingContents['letter'];
        $$('.view-question-page #view-question-step3 #lettertext').val(letter);
        $$('.view-question-page #view-question-step3 #lettertext').prop('disabled', true);
        showSteps(currentStep);
        $$('.view-question-page #view-question-step1 span#todayquestion').html(user.todayQuestion);
        $$('.view-question-page #view-question-step3 span#todayquestion').html(user.todayQuestion);
        var lefttime =  totalTime - elapsedTime;
        var strLeftTime = staticMgr.changeNumberFormat(parseInt(lefttime / 60)) + ":" + staticMgr.changeNumberFormat(elapsedTime % 60 == 0 ? 0 : 60 - (elapsedTime % 60));
        $$('.view-question-page span#lefttime').html("Timer : " + strLeftTime);
        $$('.view-question-page .btn-timer').css('visibility', 'visible');
        $$('.view-question-page .center.sliding').html('Write Dialogue Letter');
        // Should modify the timer value
        writingTimer = window.setInterval(function(){
            processTime();
        }, 1000);
        user.clearWriting();
    }

    function loadFeelingsList(){
        currentStep = 2;
        showSteps(currentStep);
        $$('.view-question-page .step-buttons').css('display', 'flex');
        $$('.view-question-page .step-buttons').css('visibility', 'visible');

        var feelingsListDOM = $$('.view-question-page .feelings-list');
        feelingsListDOM.find('li.accordion-item').remove();
        for( var i = 0; i < staticMgr.feeling_categories.length ; i++ ){
            var categoryItem = staticMgr.feeling_categories[i];
            if ( currentMood == 1 ){
                if ( categoryItem['positive'] != 'Yes' ) continue;
            }
            else if( currentMood == -1 ){
                if ( categoryItem['positive'] != 'No' ) continue;
            }
            var feelingListItem =   '<li class="accordion-item">'+
                                        '<a href="#" class="item-content item-link" data-id="' + categoryItem['id'] + '">' +
                                            '<div class="item-inner">' + 
                                                '<div class="item-title">' + categoryItem['category_content'] +'</div>' + 
                                            '</div>'+
                                        '</a>' + 
                                        '<div class="accordion-item-content">' +
                                            '<div class="content-block">' + 
                                                '<div class="list-block">'+
                                                  '<ul class="child-list">' +
                                                  '</ul>' +
                                                '</div>'+          
                                            '</div>' + 
                                        '</div>' + 
                                    '</li>';    
            feelingsListDOM.find('ul.parent-list').append(feelingListItem);
        }

        var feelingsList = staticMgr.screenHeight - staticMgr.navbarHeight - 44 - 32 * 2 - 100;
        feelingsListDOM.css('height', feelingsList + 'px');
    }

    function gotoReferencePage(){
        $$('.dialogue-helper-page .icon-only.back').css('display', 'flex');
        $$('.dialogue-helper-page a.open-panel').css('display', 'none');
        paused = true;
        helperMgr.loadPage();
    }

    function showSteps(stepNo){
        myApp.showTab('#view-question-step' + stepNo);
        $$('.view-question-page .steps-block .steps-chips .chip').removeClass('selected');
        for (var i = 1; i <= stepNo; i++){
            $$('.view-question-page .steps-block .steps-chips .chip.chip-step' + i + ' .chip-media').removeClass('bg-gray');
            $$('.view-question-page .steps-block .steps-chips .chip.chip-step' + i + ' .chip-media').addClass('bg-blue');
        }
        for (var i = stepNo + 1; i <= 4; i++){
            $$('.view-question-page .steps-block .steps-chips .chip.chip-step' + i + ' .chip-media').removeClass('bg-blue');
            $$('.view-question-page .steps-block .steps-chips .chip.chip-step' + i + ' .chip-media').addClass('bg-gray');
        }
        $$('.view-question-page .steps-block .steps-chips .chip.chip-step' + stepNo).addClass('selected');
    }

    function resumeWriting(){
        paused = false;
        $$('.view-question-page .btn-write').removeClass('color-blue');
        $$('.view-question-page .btn-write').addClass('color-red');
        $$('.view-question-page .btn-write').html('Pause');
    }
};