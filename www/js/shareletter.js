var ShareLetter = function(){

    //property
    var paused = true;
    var elapsedTime = 0;
    var totalTime = 0;
    var sharingTimer;
    var letter = "", partner_letter = "Your spouse hasn't wrote the letter yet.";
    var alarmMedia;
    // method
    this.init = init;
    this.loadPage = loadPage;
    function init(){
        initEventListeners();
    }

    function initEventListeners(){
        $$('.share-letter-page .btn-share').on('click', function(){
            paused = !paused;
            if (paused){
                $$('.share-letter-page .btn-share').addClass('color-blue');
                $$('.share-letter-page .btn-share').removeClass('color-red');
                $$('.share-letter-page .btn-share').html('Share');
            }
            else{
                $$('.share-letter-page .btn-share').removeClass('color-blue');
                $$('.share-letter-page .btn-share').addClass('color-red');
                $$('.share-letter-page .btn-share').html('Pause');
            }
        });

        $$('.share-letter-page .btn-reset').on('click', function(){
            paused = true;
            elapsedTime = 0;
            var lefttime =  totalTime - elapsedTime;
            $$('.share-letter-page span#lefttime').html(staticMgr.changeNumberFormat(parseInt(lefttime / 60)) + ":00");
            $$('.share-letter-page .btn-share').html('Share');
            $$('.share-letter-page .btn-share').addClass('color-blue');
            $$('.share-letter-page .btn-share').removeClass('color-red');
        }); 

        $$('.share-letter-page .btn-done').on('click', function(){
            window.clearInterval(sharingTimer);
            shareLetter();
        }); 

        $$('.share-letter-page .btn-mine').on('click', function () {
            /*letter = user.letterInfo['letter_content'];*/
            $$('.share-letter-page span#lettertext').html(letter);
            $$('.share-letter-page .btn-mine').removeClass('color-brown');
            $$('.share-letter-page .btn-spouse').addClass('color-brown');
        });
        $$('.share-letter-page .btn-spouse').on('click', function () {
           $$('.share-letter-page span#lettertext').html(partner_letter); 
           $$('.share-letter-page .btn-spouse').removeClass('color-brown');
           $$('.share-letter-page .btn-mine').addClass('color-brown');
        });
    }

    function shareLetter(){
        dbMgr.sendRequest( 'share_letter', {date: user.nextAppointment['date'] }, function(result){
            if (result['success']){
                myApp.alert( "Please create a new appointment.", staticMgr.appString());
                if ( user.nextAppointment['id'] != null || user.nextAppointment['id'] != undefined ){
                    dbMgr.sendRequest( 'clear_next_appointment', { id:user.nextAppointment['id'], reset: 0 }, function(result){
                        if (result['success']){
                            user.nextAppointment = {};
                            user.todayQuestion = "";
                            user.letterInfo = {};
                            homeMgr.loadPage();
                            $$('.home-page .btn-share-letter').css('display', 'none');
                        } 
                    });
                }                    
            }
            else{
                myApp.alert( result['msg'], staticMgr.appString());
            }
        });
    }

    function processTime(){
        if ( paused ) return;
        
        elapsedTime ++;

        var lefttime =  totalTime - elapsedTime;
        checkAlarmTime(lefttime);
        if (lefttime <= 0) {
            window.clearInterval(sharingTimer);
            $$('.share-letter-page .btn-share').css('visibility', 'hidden');
            $$('.share-letter-page .btn-reset').css('visibility', 'hidden');
            myApp.alert("Time is up. You are now done with the question. This becomes history.", "Dialogue Daily");
            shareLetter();
            return;
        }

        var strLeftTime = staticMgr.changeNumberFormat(parseInt(lefttime / 60)) + ":" + staticMgr.changeNumberFormat(elapsedTime % 60 == 0 ? 0 : 60 - (elapsedTime % 60));
        $$('.share-letter-page span#lefttime').html(strLeftTime);
    }

    function checkAlarmTime(lefttime){
        var alarm1Time, alarm2Time, alarm3Time;
        if (user.timerInfo['alarm1_unit'] == 'm') alarm1Time = parseInt(user.timerInfo['alarm1_time']) * 60;
        else alarm1Time = parseInt(user.timerInfo['alarm1_time']);
        if (user.timerInfo['alarm2_unit'] == 'm') alarm2Time = parseInt(user.timerInfo['alarm2_time']) * 60;
        else alarm2Time = parseInt(user.timerInfo['alarm2_time']);
        if (user.timerInfo['alarm3_unit'] == 'm') alarm3Time = parseInt(user.timerInfo['alarm3_time']) * 60;
        else alarm3Time = parseInt(user.timerInfo['alarm3_time']);

        if (alarm1Time == lefttime){
            if ( parseInt(user.timerInfo['alarm1_active']) == 1 ){
                playRing(user.timerInfo['alarm1_ring']);
            }
        } 
        else if (alarm2Time == lefttime){
            if ( parseInt(user.timerInfo['alarm2_active']) == 1 ){
                playRing(user.timerInfo['alarm2_ring']);
            }
        }
        else if (alarm3Time == lefttime){
            if ( parseInt(user.timerInfo['alarm3_active']) == 1 ){
                playRing(user.timerInfo['alarm3_ring']);
            }
        }
    }

    function playRing(ringSrc){
        var strPlatform = device.platform;
        var src = ringSrc;
        if ( src == null || src == undefined || src == "" || src == "" )
        if (strPlatform.toLowerCase() == "ios"){
            src = "audio/defaultalarm.mp3";
        }
        else if(strPlatform.toLowerCase() == "android"){
            src = "/android_asset/www/audio/defaultalarm.mp3";
        }

        alarmMedia = new Media(src, onMediaSuccess, onMediaError);
        alarmMedia.play({
            numberOfLoops: 1,
            playAudioWhenScreenIsLocked : false
        });
    }

    function onMediaSuccess(){
        alarmMedia = null;
    }

    function onMediaError(e){
        if (e.code == MediaError.MEDIA_ERR_ABORTED ){
            
            var strPlatform = device.platform;
            var src = "";
            if ( src == null || src == undefined || src == "" || src == "" )
            if (strPlatform.toLowerCase() == "ios"){
                src = "audio/defaultalarm.mp3";
            }
            else if(strPlatform.toLowerCase() == "android"){
                src = "/android_asset/www/audio/defaultalarm.mp3";
            }

            alarmMedia = new Media(src);
            alarmMedia.play({
                numberOfLoops: 1,
                playAudioWhenScreenIsLocked : false
            });
        }
    }

    function loadPage(){

        getLetters();
        paused = true;
        elapsedTime = 0;
        totalTime = parseInt(user.timerInfo['main_timer']) * 60;
        /*letter = user.letterInfo['letter_content'];*/

        // Should modify the timer value
        sharingTimer = window.setInterval(function(){
            processTime();
        }, 1000);
        $$('.share-letter-page .btn-share').css('visibility', 'visible');
        $$('.share-letter-page .btn-reset').css('visibility', 'visible');
        $$('.share-letter-page .btn-share').html('Share');
        $$('.share-letter-page .btn-share').addClass('color-blue');
        $$('.share-letter-page .btn-share').removeClass('color-red');
        /*$$('.share-letter-page span#lettertext').html(letter);*/
        $$('.share-letter-page span#todayquestion').html(user.todayQuestion);
        $$('.share-letter-page span#lefttime').html(staticMgr.changeNumberFormat(parseInt(totalTime / 60)) + ":00");

        mainView.router.load({pageName: 'share-letter'});
    }

    myApp.onPageBack('share-letter', function(page){
        window.clearInterval(sharingTimer);
        $$('.share-letter-page .btn-share').html('Share');
        $$('.share-letter-page .btn-share').addClass('color-blue');
        $$('.share-letter-page .btn-share').removeClass('color-red');
    });

    function getLetters(){
        var strday = user.letterInfo['date'];
        dbMgr.sendRequest('get_two_letters', {regdate: strday, history:'0'}, function(result){
            if (result['success']){
                var data = result['data'];
                for( var i = 0; i < data.length; i++){
                    var letterInfo = data[i];
                    if ( letterInfo['userid'] != undefined && letterInfo['userid'] != null ) {
                        if ( letterInfo['userid'] == user.userid ){
                            letter = "I feel " + letterInfo['feelings'] +" about this question.\n" + letterInfo['letter'];
                        }
                        else{
                            partner_letter = "I feel " + letterInfo['feelings'] +" about this question.\n" + letterInfo['letter'];
                        }
                    }  
                }
                $$('.share-letter-page span#lettertext').html(letter);
            }
        });
    }
};