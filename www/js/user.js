var User = function() {
    // property
    this.email;
    this.userid;

    this.userInfo = {}; // User and Contact Info
    this.timerInfo = {}; // Timer Defaults Info
    this.appointmentDefaults = {}; // Appointment Defaults Info
    this.emailWords = {}; // Email Wordings Info
    this.nextAppointment = {};
    this.todayQuestion;
    this.letterInfo = {};

    // method
    this.login = login;
    this.logout = logout;
    this.isLoggedIn = isLoggedIn;
    this.doLoginAction = doLoginAction;
    this.getUserID = getUserID;
    this.autoLogin = autoLogin;
    this.saveToLocal = saveToLocal;
    this.clearLocal = clearLocal;
    this.getLocalData = getLocalData;
    
    this.backupWriting = backupWriting;
    this.clearWriting = clearWriting;
    this.getWritingFromLocal = getWritingFromLocal;

    function login(data) {
        this.userid = data.userid;
        this.email = data.email;
        this.saveToLocal(data.email, data.password);
        $$("body").removeClass("logged-out").addClass("logged-in");
    }

    function logout() {
        this.clearLocal();
        $$("body").removeClass("logged-in").addClass("logged-out");
    }

    function isLoggedIn() {
        if(this.userid != undefined && this.userid != '') return true;
        else return false;
    }

    function doLoginAction(returnPage) {
        myApp.confirm('You need to sign in first!', function () {
            myApp.closePanel();
            options.afterLoginSuccessPage = returnPage;
            mainView.router.load({
                pageName:   'login-screen'
            });
        });
    }

    function getUserID() {
        return this.userid;
    }

    function autoLogin()
    {
        var localStorage = this.getLocalData(),
            email = localStorage["email"],
            password = localStorage["password"];

        if (email == undefined || email == null || email == '' || password == undefined || password == null || password == ''){
            setTimeout(function(){
                $$(document).trigger("AppLoaded");
            }, 4000);
        } else {
            if (!staticMgr.validateEmail(email)){
                myApp.alert("Invalid Email address", "Log In");
            }
            else{
                dbMgr.sendRequest('login', {email: email, password: password}, function(result){
                    if (result['success']) {
                        var userInfo = JSON.parse(JSON.stringify(result['data']));
                        user.userInfo = JSON.parse(JSON.stringify(result['data']));
                        user.login({email: email, password: password, userid: userInfo['id'] });
                        if ( user.userInfo['level'] == '2' ){
                            $$('.list-block.menu-list a#buy-now-page-link').css('display', 'none');
                        }
                        dbMgr.sendRequest('feeling_categories', {} , function(result){
                            staticMgr.feeling_categories = JSON.parse(JSON.stringify(result['data']));
                        });

                        dbMgr.sendRequest('question_categories', {} , function(result){
                            staticMgr.question_categories = JSON.parse(JSON.stringify(result['data']));
                        });

                        if (( userInfo['phone'] != '' && userInfo['partner_email'] != '' && userInfo['partner_name'] != '' && userInfo['partner_phone'] != '' ) && 
                            ( userInfo['phone'] != null && userInfo['partner_email'] != null && userInfo['partner_name'] != null && userInfo['partner_phone'] != null ) && 
                            ( userInfo['phone'] != undefined && userInfo['partner_email'] != undefined && userInfo['partner_name'] != undefined && userInfo['partner_phone'] != undefined )){
                            if ( user.appointmentDefaults['id'] == null || user.appointmentDefaults['id'] == undefined ){
                                dbMgr.sendRequest('get_appointment_defaults', {}, function(result){
                                    if ( result['success'] ){
                                        var data = result['data'];
                                        user.appointmentDefaults = JSON.parse(JSON.stringify(data));
                                        homeMgr.loadPage();
                                    }
                                    else{
                                        myApp.alert('Failed to get appointment defaults.' , 'Dialogue Daily');
                                    }
                                });
                            }
                        }   
                        else {
                            contactInfoMgr.loadPage();
                        }
                    }
                    else{
                        myApp.alert( result['msg'], 'Log In' );
                    }
                    $$(document).trigger("AppLoaded");
                });
            }
        };
    }

    function saveToLocal(email, password) {
        window.localStorage["email"] = email;
        window.localStorage["password"] = password;
    }

    function clearLocal() {
        this.email = '';
        this.userid = '';
        this.userInfo = {}; // User and Contact Info
        this.timerInfo = {}; // Timer Defaults Info
        this.appointmentDefaults = {}; // Appointment Defaults Info
        this.emailWords = {}; // Email Wordings Info
        this.nextAppointment = {};
        this.letterInfo = {};
        this.writingContents = {};
        delete window.localStorage["email"];
        delete window.localStorage["password"];
    }

    function getLocalData() {
        var data = {
            "email": window.localStorage["email"],
            "password": window.localStorage["password"],
        };
        return data;
    }

    function getWritingFromLocal(){
        var writing = {
            totalTime: window.localStorage["totalTime"],
            elapsedTime: window.localStorage["elapsedTime"],
            questionid: window.localStorage["questionid"],
            question: window.localStorage["question"],
            feelings: window.localStorage["feelings"],
            letter: window.localStorage["letter"],
            date: window.localStorage["date"],
            mood: window.localStorage["mood"]
        };
        if (writing['date'] == undefined || writing['date'] == null) return false;
        return writing;    
    }
    
    function backupWriting(writingContents){
        var writing = JSON.parse(JSON.stringify(writingContents));
        window.localStorage["totalTime"] = writing['totalTime'];
        window.localStorage["elapsedTime"] = writing['elapsedTime'];
        window.localStorage["questionid"] = writing['questionid'];
        window.localStorage["question"] = writing['question'];
        window.localStorage["feelings"] = writing['feelings'];
        window.localStorage["letter"] = writing['letter'];
        window.localStorage["date"] = writing['date'];
        window.localStorage["mood"] = writing['mood'];
    }

    function clearWriting(){
        delete window.localStorage["totalTime"];
        delete window.localStorage["elapsedTime"];
        delete window.localStorage["questionid"];
        delete window.localStorage["question"];
        delete window.localStorage["feelings"];
        delete window.localStorage["letter"];
        delete window.localStorage["date"];
        delete window.localStorage["mood"];
    }
}

var user = new User();