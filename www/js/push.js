document.addEventListener("deviceready", addPushNotification, false);

function addPushNotification() {
    //-----------------
    //-- Push notification
    var deviceUuId = device.uuid;
    var deviceType = device.platform; 

    var push = PushNotification.init({
        "android": {
            "senderID": "709234682456"
        },
        "ios": {"alert": "true", "badge": "true", "sound": "true"}, 
        "windows": {} 
    });
    push.on('registration', function(data) {
        var registrationId = data.registrationId;
        options.token = {
            registrationId: registrationId,
            deviceUuId: deviceUuId,
            deviceType: deviceType
        };
        $$(document).trigger('tokenInitiated');
        
        $$.ajax({
            url: options.api_url + "api/tokenAdd",
            type: "POST",
            data:"registrationId=" + registrationId + "&deviceUuId=" + deviceUuId + "&deviceType=" + deviceType
        });
    });

    push.on('notification', function(data) {
    });

    push.on('error', function(e) {
        console.log("push error");
    });
}