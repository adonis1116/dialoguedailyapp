// System Emaili Wordings Controller

var SystemEmailWordings = function(){

    //property
    var tempWordingsInfo;
    //method
    this.init = init;
    this.loadPage = loadPage;

    function init(){
        initEventListeners();
    }

    function initEventListeners(){
        $$(document).on('click', '.system-email-page .btn-save-words', function(){
            saveEmailWordings();
		});
    }

    function loadPage(){
        mainView.router.load({pageName: 'system-email'});
        loadDataFromDB();
    }

    function loadDataFromDB(){
        if ( user.emailWords['id'] == null || user.emailWords['id'] == undefined ){
            dbMgr.sendRequest('get_email_wordings', {}, function(result){
                if ( result['success'] ){
                    var data = result['data'];
                    user.emailWords = data;
                    tempWordingsInfo = data;
                    layoutEmailWordings();
                }
                else{
                    myApp.alert('Failed to get Email Wordings.' , 'Email Wordings');
                }
            });
        }
        else {
            tempWordingsInfo = user.emailWords;
            layoutEmailWordings();
        }
    }

    function layoutEmailWordings(){
        if (user.emailWords == null || user.emailWords == undefined) return;

        $$('.system-email-page #appointment').val(user.emailWords['appointment']);
        $$('.system-email-page #done').val(user.emailWords['done']);
        $$('.system-email-page #share').val(user.emailWords['share']);


        $$('.system-email-page .list-block.inputs-list .item-inner').addClass('not-empty-state');
        $$('.system-email-page .list-block.inputs-list .item-inner .item-input').addClass('not-empty-state');
        $$('.system-email-page .list-block.inputs-list .item-inner .item-input input').addClass('not-empty-state');
    }

    function saveEmailWordings(){
        tempWordingsInfo['appointment'] = $$('.system-email-page #appointment').val();
        tempWordingsInfo['done'] = $$('.system-email-page #done').val();
        tempWordingsInfo['share'] = $$('.system-email-page #share').val();
        dbMgr.sendRequest('update_email_wordings', tempWordingsInfo, function(result){
            if (result['success']){
                user.emailWords = tempWordingsInfo;                    
                myApp.alert(result['msg'], staticMgr.appString());
                mainView.router.back();
            }
            else{
                myApp.alert(result['msg'], staticMgr.appString());
            }
        });
    }
};