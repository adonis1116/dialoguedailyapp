//Timer Defaults Controllers
var TimerDefaults = function(){

	// property
	var MAX_ALARM_COUNT; // Maximum Alarm Count
	var timePicker;
    var isPlaying1, isPlaying2, isPlaying3;
    var alarmMedia1, alarmMedia2, alarmMedia3;
    var selectedMedia = 0;
	this.tempTimerData;

	// method
	this.init = init;
	this.loadPage = loadPage;
	this.layoutAlarms = layoutAlarms;

	// Initialize the class
	function init(){
		MAX_ALARM_COUNT = 3;
		tempTimerUnit = 'm';
		tempTimerTime = 10;
		alarmMedia1 = null;
		alarmMedia2 = null;
		alarmMedia3 = null;
		isPlaying1 = false;
		isPlaying2 = false;
		isPlaying3 = false;
		initEventListeners();
	}

	function initEventListeners(){
		$$(document).on('click', '.timer-defaults-page .btn-save-timer', function(){
			// Save Timer to Database
			saveTimerDefaults();
		});

		$$(document).on('click', '.timer-defaults-page .setTime', function(){
			timePicker.open();
		});

		$$(document).on('click', '.timer-defaults-page .alarm-cards .card .alarmedit', function(){
			var index = $$(this).data('id');
			stopAllMedia();
			alarmController.loadPage(index);
		});

		$$(document).on('click', '.timer-defaults-page .alarm-cards .card .alarmring', function(){
			var index = $$(this).data('id');
			var src = timerDefaultsMgr.tempTimerData['alarm' + index + '_ring'];
			
			if (src == undefined || src == null){
                myApp.alert("Please choose a audio for alarm.", "Dialogue Daily");
                return;
            }

            var strPlatform = device.platform;
            if ( src == "default" || src == "" ){
            	if (strPlatform.toLowerCase() == "ios"){
					src = "audio/defaultalarm.mp3";
            	}
            	else if(strPlatform.toLowerCase() == "android"){
            		src = "/android_asset/www/audio/defaultalarm.mp3";
            	}
            }
			
			index = parseInt(index);
			selectedMedia = index;
            switch(index){
            	case 1:
		            if (isPlaying1){
						if (alarmMedia1) alarmMedia1.stop();
            			alarmMedia1 = null;
            			$$(this).html('Play');
		            }
		            else {
		            	stopAllMedia();
		            	changeButtonStatus(index);
		                alarmMedia1 = new Media(src, mediaSuccess, mediaError);
		                alarmMedia1.play({
		                	numberOfLoops: 1,
		                	playAudioWhenScreenIsLocked : false
		                });
		            }

		            isPlaying1 = !isPlaying1;
            	break;
            	case 2:
            		if (isPlaying2){
            			if (alarmMedia2) alarmMedia2.stop();
            			alarmMedia2 = null;
            			$$(this).html('Play');
            		}
            		else{
            			stopAllMedia();
		            	changeButtonStatus(index);
		                alarmMedia2 = new Media(src, mediaSuccess, mediaError);
		                alarmMedia2.play({
		                	numberOfLoops: 1,
		                	playAudioWhenScreenIsLocked : false
		                });
		            }

		            isPlaying2 = !isPlaying2;
            	break;
            	case 3:
            		if (isPlaying3){
						if (alarmMedia3) alarmMedia3.stop();
            			alarmMedia3 = null;
            			$$(this).html('Play');
            		}
            		else {
            			stopAllMedia();
		            	changeButtonStatus(index);
		                alarmMedia3 = new Media(src, mediaSuccess, mediaError);
		                alarmMedia3.play({
		                	numberOfLoops: 1,
		                	playAudioWhenScreenIsLocked : false
		                });
		            }

		            isPlaying3 = !isPlaying3;
            	break;
            }
            
		});

	}

	function mediaSuccess(){
		stopAllMedia();
	}

	function mediaError(e){
		if (e.code == MediaError.MEDIA_ERR_ABORTED ){
			var src = "";
	        var strPlatform = device.platform;
	    	if (strPlatform.toLowerCase() == "ios"){
				src = "audio/defaultalarm.mp3";
	    	}
	    	else if(strPlatform.toLowerCase() == "android"){
	    		src = "/android_asset/www/audio/defaultalarm.mp3";
	    	}

	        stopAllMedia();
			changeButtonStatus(selectedMedia);
	        switch(selectedMedia){
	            case 1:
                    if (!alarmMedia1){
		        		alarmMedia1 = new Media(src);
		        		alarmMedia1.play({
			            	numberOfLoops: 1,
			            	playAudioWhenScreenIsLocked : false
			            });
		        	}
	                isPlaying1 = !isPlaying1;
	            break;
	            case 2:
                    if (!alarmMedia2){
						alarmMedia2 = new Media(src);
						alarmMedia2.play({
			            	numberOfLoops: 1,
			            	playAudioWhenScreenIsLocked : false
			            });
					}
	                isPlaying2 = !isPlaying2;
	            break;
	            case 3:
                	if (!alarmMedia3){
	                	alarmMedia3 = new Media(src);
			        	alarmMedia3.play({
			            	numberOfLoops: 1,
			            	playAudioWhenScreenIsLocked : false
			            });	
                	}
	                isPlaying3 = !isPlaying3;
	            break;
	        }
		}
	}

	function stopAllMedia(){
		if (alarmMedia1) alarmMedia1.stop();
    	if (alarmMedia2) alarmMedia2.stop();
    	if (alarmMedia3) alarmMedia3.stop();
    	alarmMedia1 = null;
		alarmMedia2 = null;
		alarmMedia3 = null;
		isPlaying1 = false;
		isPlaying2 = false;
		isPlaying3 = false;
		$$('.timer-defaults-page .alarm-cards .card .alarmring').html('Play');
	}

	function changeButtonStatus(index){
		$$('.timer-defaults-page .alarm-cards .card .alarmring').html('Play');
		$$('.timer-defaults-page .alarm-cards .card.alarm' + index + ' .alarmring').html('Stop');
		if (index == 1){
			if (alarmMedia2) alarmMedia2.stop();
			if (alarmMedia3) alarmMedia3.stop();
			alarmMedia2 = null;
			alarmMedia3 = null;
		}
		else if (index == 2){
			if (alarmMedia1) alarmMedia1.stop();
			if (alarmMedia3) alarmMedia3.stop();
			alarmMedia1 = null;
			alarmMedia3 = null;	
		}
		else if (index == 3){
			if (alarmMedia1) alarmMedia1.stop();
			if (alarmMedia2) alarmMedia2.stop();
			alarmMedia1 = null;
			alarmMedia2 = null;
		}
	}

	// Retrieve data from Server
	function loadPage(){
		initTimerDOM();
		loadTimerDefaults();
		mainView.router.load({pageName: 'timer-defaults'});
	}

	myApp.onPageBack('timer-defaults', function(page){
        stopAllMedia();
    });

	function loadTimerDefaults(){
		if ( user.timerInfo['id'] == undefined || user.timerInfo['id'] == null )
		{
			dbMgr.sendRequest( 'get_timer', {} , function(result){
				if ( result['success'] ){
					user.timerInfo = JSON.parse(JSON.stringify(result['data']));
					timerDefaultsMgr.tempTimerData = JSON.parse(JSON.stringify(result['data']));
					layoutAlarms();
				}
				else{
					myApp.alert('Failed to get timer defaults.' , 'Timer Defaults');
				}
			});
		}
		else{
			timerDefaultsMgr.tempTimerData = user.timerInfo;
			layoutAlarms();
		}
	}

	function initTimerDOM(){
		var strValue = "";
	    for ( var i = 1; i <= 60; i++)
	    {
	        if (i != 60) strValue += i + ' ';
	        else if (i == 60)strValue += i;
	    }
	    var newCols = [];
	    newCols.push({
	        textAlign: 'left',
	        values: strValue.split(' ')
	    });

	    newCols.push({
	        textAlign: 'right',
	        values: ['Minutes']
	    });
        timePicker = myApp.picker({
	        input: '#timerAmount',
	        rotateEffect: true,
	        closeByOutsideClick: true,
	        cols: newCols,
	        onChange: function(p, values, displayValues){
	        	$$('.timer-defaults-page .setTime').html( values[0] + ' ' + values[1] );
	        	timerDefaultsMgr.tempTimerData['main_timer'] = values[0];
	        	timerDefaultsMgr.tempTimerData['main_timer_unit'] = 'm';
	        }
	    });
	}

	function layoutAlarms(){
		$$('.timer-defaults-page .setTime').html( timerDefaultsMgr.tempTimerData['main_timer'] + ' ' + ( timerDefaultsMgr.tempTimerData['main_timer_unit'] == 'm' ? 'Minutes' : 'Seconds'));
		$$('.timer-defaults-page .alarm-cards .card .alarmring').html('Play');
		for ( var i = 1; i <= MAX_ALARM_COUNT; i++ ){
			var alarmItem = {
				isActive: ( timerDefaultsMgr.tempTimerData['alarm' + i + '_active'] == '1' ? true : false),
				time: timerDefaultsMgr.tempTimerData['alarm' + i + '_time'],
				unit: timerDefaultsMgr.tempTimerData['alarm' + i + '_unit'],
				ring: timerDefaultsMgr.tempTimerData['alarm' + i + '_ring']
			};
			var alarmCard = $$('.timer-defaults-page .alarm-cards .card.alarm.alarm' + i );
			alarmCard.find('.alarmtime').html(alarmItem['time'] + ' ' + ( alarmItem['unit'] == 'm' ? 'Minutes' : 'Seconds'));
			alarmCard.find('.alarmactive').prop('checked', alarmItem['isActive']);
		}
		isPlaying1 = false;
		isPlaying2 = false;
		isPlaying3 = false;
	}

	function saveTimerDefaults(){

		for ( var i = 1; i <= MAX_ALARM_COUNT; i++ ){
			var alarmCard = $$('.timer-defaults-page .alarm-cards .card.alarm.alarm' + i );
			timerDefaultsMgr.tempTimerData['alarm' + i + '_active'] = ( alarmCard.find('.alarmactive').prop('checked') == true ? 1 : 0);
		}
		dbMgr.sendRequest('update_timer', timerDefaultsMgr.tempTimerData, function(result){
			if (result['success']){
				user.timerInfo = JSON.parse(JSON.stringify(timerDefaultsMgr.tempTimerData));;
				myApp.alert(result['msg'], staticMgr.appString());
				mainView.router.back();
			}
			else{
				myApp.alert(result['msg'], staticMgr.appString());
			}
		});
	}
};

var alarmController = new AlarmController();
alarmController.init();