
// Alarm Controller
var AlarmController = function(){
    //property
    var alarmPicker;
    var currentAlarmTime;
    var currentAlarmUnit;
    var currentMediaSrc;
    var alarmId = 0;
    var alarmMedia;
    var isPlaying = false;
    //methods

    this.init = init;
    this.loadPage = loadPage;
    function init(){
        initEventListeners();
        currentAlarmTime = 10;
        currentAlarmUnit = 'm';
        currentMediaSrc = "";
    }

    function loadPage(id){
        alarmId = id;
        isPlaying = false;
        alarmMedia = null;
        initDOM();
        mainView.router.load({pageName: 'edit-alarm'});
    }

    function initAlarmPicker(){
        var strValue = "";
        for ( var i = 1; i <= 60; i++)
        {
            if (i != 60) strValue += i + ' ';
            else if (i == 60)strValue += i;
        }
        var newCols = [];
        newCols.push({
            textAlign: 'left',
            values: strValue.split(' ')
        });

        newCols.push({
            textAlign: 'right',
            values: ['Minutes', 'Seconds']
        });

        alarmPicker = myApp.picker({
            input: '#alarmAmount',
            rotateEffect: true,
            closeByOutsideClick: true,
            cols: newCols,
            onChange: function(p, values, displayValues){
                $$('.edit-alarm-page .setTime').html( values[0] + ' ' + values[1] );
                currentAlarmTime = p.cols[0].values.indexOf(values[0]) + 1;
                currentAlarmUnit = p.cols[1].values.indexOf(values[1]) == 0 ? 'm' : 's';
            }
        });   
    }

    function initEventListeners(){
        initAlarmPicker();
        $$(document).on('click', '.edit-alarm-page .btn-edit-alarm', function(){

            timerDefaultsMgr.tempTimerData['alarm' + alarmId + '_time'] = currentAlarmTime;
            timerDefaultsMgr.tempTimerData['alarm' + alarmId + '_unit'] = currentAlarmUnit;
            timerDefaultsMgr.tempTimerData['alarm' + alarmId + '_ring'] = currentMediaSrc;
            if (isPlaying && alarmMedia){
                alarmMedia.stop();
            }
            alarmMedia = null;
            isPlaying = false;
            $$('.edit-alarm-page .btn-play-audio').html("Play");
            mainView.router.back();
        });

        myApp.onPageBack('edit-alarm', function(page){
            $$('.edit-alarm-page .btn-play-audio').html("Play");

            if (isPlaying && alarmMedia){
                alarmMedia.stop();
            }
            alarmMedia = null;
            isPlaying = false;

            timerDefaultsMgr.layoutAlarms();
        });

        $$(document).on('click', '.edit-alarm-page .setTime', function(){
            alarmPicker.open();
        });

        $$(document).on('click', '.edit-alarm-page .btn-play-audio', function(){

            if (currentMediaSrc == undefined || currentMediaSrc == null){
                myApp.alert("Please choose a audio for alarm.", "Dialogue Daily");
                return;
            }
            
            var strPlatform = device.platform;
            if ( currentMediaSrc == "default" || currentMediaSrc == "" ){
                if (strPlatform.toLowerCase() == "ios"){
                    currentMediaSrc = "audio/defaultalarm.mp3";
                }
                else if(strPlatform.toLowerCase() == "android"){
                    currentMediaSrc = "/android_asset/www/audio/defaultalarm.mp3";
                }
            }

            if (alarmMedia){
                alarmMedia.stop();   
                alarmMedia = null;
            }
            
            if (isPlaying){
                $$(this).html("Play");
            }
            else{
                alarmMedia = new Media(currentMediaSrc, onMediaSuccess, onMediaError);
                $$(this).html("Stop");
                alarmMedia.play({
                    numberOfLoops: 1,
                    playAudioWhenScreenIsLocked : false
                });
            }
            isPlaying = !isPlaying;
        });

        $$(document).on('click', '.edit-alarm-page .btn-choose-audio', function(){

            window.plugins.mediapicker.getAudio(function(data){
                currentMediaSrc = data[0]['exportedurl'];
            },function(e){
            }, false, false, "Select a audio file");

        });
    }

    function onMediaSuccess(){
        $$('.edit-alarm-page .btn-play-audio').html("Play");
        alarmMedia = null;
    }

    function onMediaError(e){
        if (e.code == MediaError.MEDIA_ERR_ABORTED ){
            var strPlatform = device.platform;
            if (strPlatform.toLowerCase() == "ios"){
                currentMediaSrc = "audio/defaultalarm.mp3";
            }
            else if(strPlatform.toLowerCase() == "android"){
                currentMediaSrc = "/android_asset/www/audio/defaultalarm.mp3";
            }
            isPlaying = false;
            alarmMedia = null;
            if (!alarmMedia){
                alarmMedia = new Media(currentMediaSrc);
                alarmMedia.play({
                    numberOfLoops: 1,
                    playAudioWhenScreenIsLocked : false
                });
            }
            isPlaying = !isPlaying;
        }
    }

    function initDOM(){
        currentAlarmTime = timerDefaultsMgr.tempTimerData['alarm' + alarmId + '_time'];
        currentAlarmUnit = timerDefaultsMgr.tempTimerData['alarm' + alarmId + '_unit'];
        currentMediaSrc = timerDefaultsMgr.tempTimerData['alarm' + alarmId + '_ring'];
        $$('.edit-alarm-page .setTime').html(currentAlarmTime + ' ' + (currentAlarmUnit == 'm' ? 'Minutes' : 'Seconds'));
    }
}