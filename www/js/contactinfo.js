//Contact Info Controllers
var ContactInfo = function(){

	var name = '',
		email = '',
		phone = '',
		partner_name = '',
		partner_email = '',
		partner_phone = '';

	// method
	this.init = init;
	this.loadPage = loadPage;

	// Initialize the class
	function init(){
		initEventListeners();
	}

	function initEventListeners(){
		$$(document).on('click', '.contact-info-page .btn-save-info', function(){
			
			// Save Contact Info to Database
			if ( validateForm() ){

				var contactInfo  = {
					name : name,
					phone : phone,
					email : email,
					partner_email : partner_email,
					partner_name : partner_name,
					partner_phone : partner_phone
				};

				dbMgr.sendRequest('update_contact', contactInfo, function(result){
					if (result['success']) {
						user.userInfo['first_name'] = contactInfo['name'];
						user.userInfo['phone'] = contactInfo['phone'];
						user.userInfo['email'] = contactInfo['email'];
						user.userInfo['partner_email'] = contactInfo['partner_email'];
						user.userInfo['partner_name'] = contactInfo['partner_name'];
						user.userInfo['partner_phone'] = contactInfo['partner_phone'];
						myApp.alert( result['msg'], 'Dialogue Daily' );
						
						if ( user.appointmentDefaults['id'] == null || user.appointmentDefaults['id'] == undefined ){
							staticMgr.isFirstLoad = true;
							appointmentDefaultsMgr.loadPage();
				        	mainView.router.load({pageName:'appointment-defaults'});    
				        }
				        else{
				        	mainView.router.back();
				    	}	
                    }
                    else{
                        myApp.alert( result['msg'], 'Dialogue Daily' );
                    }
				});
			}
		});
	}

	// Retrieve data from Server
	function loadPage(){
        loadContactInfo();
        mainView.router.load({pageName: 'contact-info'});
	}

	function validateForm(){

		name = $$('.contact-info-page #name').val();
		email = $$('.contact-info-page #email').val();
		phone = $$('.contact-info-page #phone').val();
		partner_name = $$('.contact-info-page #partner_name').val();
		partner_email = $$('.contact-info-page #partner_email').val();
		partner_phone = $$('.contact-info-page #partner_phone').val();

		if ( name == '' || partner_name == '' || email == '' || partner_email == '' || phone == '' || partner_phone == ''){
			myApp.alert("Should not be empty.", "Contact Info");
			return false;
		}

		if ( !staticMgr.validateEmail(email) || !staticMgr.validateEmail(partner_email) ){
			myApp.alert("Invalid Email Address", "Contact Info");
			return false;	
		}

		return true;
	}

	function loadContactInfo(){

		if (user.userInfo == null || user.userInfo == undefined) return;

		$$('.contact-info-page #name').val(user.userInfo['first_name']);
		$$('.contact-info-page #email').val(user.userInfo['email']);
		$$('.contact-info-page #phone').val(user.userInfo['phone']);
		$$('.contact-info-page #partner_name').val(user.userInfo['partner_name']);
		$$('.contact-info-page #partner_email').val(user.userInfo['partner_email']);
		$$('.contact-info-page #partner_phone').val(user.userInfo['partner_phone']);
		
		$$('.contact-info-page .list-block.inputs-list .item-inner').addClass('not-empty-state');
		$$('.contact-info-page .list-block.inputs-list .item-inner .item-input').addClass('not-empty-state');
		$$('.contact-info-page .list-block.inputs-list .item-inner .item-input input').addClass('not-empty-state');

	}

};