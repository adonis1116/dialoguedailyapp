// Sign in button
myApp.onPageInit('login-screen', function (page) {
    var userData = user.getLocalData();
    var email = userData["email"],
        password = userData["password"];
    $$(page.container).find("a.submit-button").click(function(){
        var formData = myApp.formToJSON('#login_form'),
        	email = formData.email,
        	password = formData.password;

		if (email == '' || password == '') {
			myApp.alert("Email or Password should not be empty.", "Log In");
		} else {
            if (!validateEmail(email)){
                myApp.alert("Invalid Email address", "Log In");
            }
            else{
                dbMgr.sendRequest('login', {email: email, password: password}, function(result){
                    if (result['success']) {
                        var userInfo = JSON.parse(JSON.stringify(result['data']));
                        user.userInfo = JSON.parse(JSON.stringify(result['data']));
                        if ( user.userInfo['level'] == '2' ){
                            $$('.list-block.menu-list a#buy-now-page-link').css('display', 'none');
                        }
                        user.login({email: email, password: password, userid: userInfo['id'] });
                        
                        dbMgr.sendRequest('feeling_categories', {} , function(result){
                            staticMgr.feeling_categories = JSON.parse(JSON.stringify(result['data']));
                        });

                        dbMgr.sendRequest('question_categories', {} , function(result){
                            staticMgr.question_categories = JSON.parse(JSON.stringify(result['data']));
                        });

                        if (( userInfo['phone'] != '' && userInfo['partner_email'] != '' && userInfo['partner_name'] != '' && userInfo['partner_phone'] != '' ) && 
                            ( userInfo['phone'] != null && userInfo['partner_email'] != null && userInfo['partner_name'] != null && userInfo['partner_phone'] != null ) && 
                            ( userInfo['phone'] != undefined && userInfo['partner_email'] != undefined && userInfo['partner_name'] != undefined && userInfo['partner_phone'] != undefined )){
                            if ( user.appointmentDefaults['id'] == null || user.appointmentDefaults['id'] == undefined ){
                                dbMgr.sendRequest('get_appointment_defaults', {}, function(result){
                                    if ( result['success'] ){
                                        var data = result['data'];
                                        user.appointmentDefaults = JSON.parse(JSON.stringify(data));
                                        homeMgr.loadPage();
                                    }
                                    else{
                                        myApp.alert('Failed to get appointment defaults.' , 'Dialogue Daily');
                                    }
                                });
                            }
                            
                        }   
                        else {
                            contactInfoMgr.loadPage();
                        }
                    }
                    else{
                        myApp.alert( result['msg'], 'Log In' );
                    }
                });
            }
		};
    });
});

var delayMillis = 1000; //1 second

setTimeout(function() {
  //your code to be executed after 1 second
}, delayMillis);

// Sign up button
myApp.onPageInit('login-signup-screen', function (page) {
    $$(page.container).find("a.submit-button").click(function(){
        var formData = myApp.formToJSON('#signup_form'),
            /*username = formData.username,*/
            email = formData.email,
            password = formData.password,
            repassword = formData.repassword;

        function validate() {
            if (email == '') {
                return "Email should not be empty.";
            } else {
                var emailExp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (!email.match(emailExp)) {
                    return "Invalid email";
                }
            }

            if (password == '') {
                return "Password should not be empty.";
            }

            if (repassword == '') {
                return "Please confirm password.";
            } 

            if (password != repassword) {
                return "Password does not match the confirm password.";
            }
            
            return true;
        }

        var error = validate();

        if (email == '' || password == '') {
            myApp.alert("Email or Password should not be empty.", "Log In");
        } else {
            if (!validateEmail(email)){
                myApp.alert("Invalid Email address", "Log In");
            }
            else{
                dbMgr.sendRequest('signup', {password: password, email: email}, function(result){
                    if (result['success']) {
                        user.login({email: email, password: password});
                        mainView.router.load({pageName:'login-screen'});
                    }
                    else{
                        myApp.alert( result['data']['msg'], 'Sign Up' );
                    }
                });
            }
        }
    });
});

// Login Recover button
myApp.onPageInit('login-recover-screen', function (page) {
    $$(page.container).find("a.submit-button").click(function(){
        var formData = myApp.formToJSON('#recover_form'),
            email = formData.email;

        if (email == '') {
            myApp.alert("Email or Password should not be empty.", "Dialogue Daily");
        } else {
            if (!validateEmail(email)){
                myApp.alert("Invalid Email address", "Dialogue Daily");
            }
            else{
                dbMgr.sendRequest('recover', { email: email }, function(result){
                    if (result['success']) {
                        mainView.router.load({pageName:'login-screen'});
                    }
                    myApp.alert( result['msg'], 'Dialogue Daily' );
                });
            }
        }
    });
    $$(page.container).find('.btn-back').click(function(){
        mainView.router.load({pageName:'login-screen'});
    });
});

$$(document).on("click", ".logout-link", function() {
	myApp.confirm('Are you sure?', 'Sign Out', function () {
        user.logout();
        mainView.router.load({pageName:'login-screen'});
        myApp.closePanel();
    });
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}