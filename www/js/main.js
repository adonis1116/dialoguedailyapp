// Initialize your app
var myApp = new Framework7({
    modalTitle: 'Dialogue Daily',
    // Enable Material theme
    material: true,
    init:false,
    preroute: function (view, options) {
        var pageName = options.pageName;
        myApp.closePanel();
    },
    onPageAfterAnimation: function(app, page) {
        var pageName = page.name;
        // Save Latest Page for Signin/Signup page
        if(pageName != 'login-screen' && pageName != 'login-signup-screen') {
            options.latestPage = pageName;
            // Activate current menu item
            if($$(".menu-list.list-block #"+pageName+"-page-link").length>0) {
                $$(".menu-list.list-block .item-link").removeClass("active");
                $$(".menu-list.list-block #"+pageName+"-page-link").addClass("active");
            }
        }
    },
    onPageBack: function(app, page){
        if ( page.name == 'create-question' ){
            var strDate;
            var strWritingTime;
            var strSharingTime;
            var strQuestion;
            $$('.list-block.menu-list a.clear-link').css('display', 'block');
            if ( user.nextAppointment['id'] != null || user.nextAppointment['id'] != undefined ){
                $$('.home-page .btn-view-question').css('visibility', 'visible');
                $$('.home-page .btn-create-question').css('visibility', 'hidden');
                strDate = user.nextAppointment['date'];
                var arrayWritingTime = (user.nextAppointment['writing_time']).split(' ');
                var arraySharingTime = (user.nextAppointment['sharing_time']).split(' ');
                strWritingTime = staticMgr.changeNumberFormat(arrayWritingTime[0]) + ':' + staticMgr.changeNumberFormat(arrayWritingTime[1]) + arrayWritingTime[2] ;
                strSharingTime = staticMgr.changeNumberFormat(arraySharingTime[0]) + ':' + staticMgr.changeNumberFormat(arraySharingTime[1]) + arraySharingTime[2] ;

                dbMgr.sendRequest( 'get_question_by_id', {id: user.nextAppointment['questionid']}, function(result){
                    if (result['data']['success']){
                        var data = JSON.parse(JSON.stringify(result['data']['data']));
                        strQuestion = data['content'];
                        $$('.home-page #next-question').html(strQuestion);
                        $$('.home-page .btn-view-question').css('display', 'block');
                        $$('.home-page .btn-create-question').css('display', 'none');
                        $$('.home-page .btn-reset-appointment').css('display', 'none');
                        $$('.home-page .btn-home').css('display', 'flex');

                        user.todayQuestion = strQuestion;
                    }
                    else{
                        myApp.alert(result['msg'], staticMgr.appString());
                    }
                });
            }
            else{
                $$('.home-page .btn-view-question').css('visibility', 'hidden');
                $$('.home-page .btn-create-question').css('visibility', 'visible');
                strDate = 'None';
                strWritingTime = 'None';
                strSharingTime = 'None';
                strQuestion = 'None';
                user.todayQuestion = "";
                $$('.list-block.menu-list a.clear-link').css('display', 'none');
            }
            $$('.home-page #appointment-date').html(strDate);
            $$('.home-page #appointment-writing').html(strWritingTime);
            $$('.home-page #appointment-sharing').html(strSharingTime);    
            $$('.home-page #next-question').html(strQuestion); 
        }
        else if( page.name == 'view-question' ){
            if ( user.letterInfo['userid'] != null || user.letterInfo['userid'] != undefined){
                $$('.home-page .btn-view-question').css('display', 'none');
                $$('.home-page .btn-create-question').css('display', 'none');
                $$('.home-page .btn-share-letter').css('display', 'block');
            }
        }
        else if ( page.name == 'dialogue-helper' ){
            $$('.dialogue-helper-page .btn-home').css('visibility', 'visible');
            writeLetterMgr.resumeWriting();
        }

    }
});

// Export selectors engine
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    dynamicNavbar: true, // Because we use fixed-through navbar we can enable dynamic navbar
    domCache: true //enable inline pages
});

setTimeout(function(){
    myApp.init();
}, 150); 

document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
    // In App Browser
    window.open = cordova.InAppBrowser.open;
    // Resume Event
    document.addEventListener("resume", function(){
        $$(document).trigger("resume");
    }, false);
    // Deplay splashcreen 
    /*$$(document).on("AppLoaded", function(){    // Disable in android build
        navigator.splashscreen.hide();
        
    });*/
    // Auto-login
    user.autoLogin();

    AppRate.preferences = {
        usesUntilPrompt: 3,
        promptAgainForEachNewVersion: true,
        inAppReview: false,
        storeAppURL: {
            ios: '1273605051',
            android: 'market://details?id=com.awsns.dialoguedaily',
        },
        customLocale: {
            title: "%@",
            message: "Thank you very much for using Dialogue Daily. We expect your support!",
            cancelButtonLabel: "No, Thanks",
            laterButtonLabel: "Remind Me Later",
            rateButtonLabel: "Rate It Now",
            yesButtonLabel: "Yes!",
            noButtonLabel: "Not really",
            appRatePromptTitle: 'Do you like using %@',
        },
        callbacks: {
            /*handleNegativeFeedback: function(){
                window.open('mailto:feedback@example.com','_system');
            },*/
            onRateDialogShow: function(callback){
                return;
            },
            onButtonClicked: function(buttonIndex){
            }
        }
    };

    AppRate.promptForRating(false);

    var productIds = ['com.awsns.dialoguedaily.yearly', 'com.awsns.dialoguedaily.lifetime'];
    inAppPurchase
        .getProducts(productIds)
        .then(function (products) {
        })
        .catch(function (err) {
            myApp.alert("Maybe you can not upgrade your account now.", "Dialogue Daily");
        });

}

var dbMgr = new DBMgr();
dbMgr.init();

var staticMgr = new StaticManager();
staticMgr.init();