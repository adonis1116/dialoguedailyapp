document.addEventListener("deviceready", addPushNotification, false);

function addPushNotification() {
    //-----------------
    //-- Push notification
    var deviceUuId = device.uuid;
    var deviceType = device.platform; 

    var push = PushNotification.init({
        "android": {
            "senderID": "709234682456"
        },
        "ios": {"alert": "true", "badge": "true", "sound": "true"}, 
        "windows": {} 
    });
    push.on('registration', function(data) {
        var registrationId = data.registrationId;
        options.token = {
            registrationId: registrationId,
            deviceUuId: deviceUuId,
            deviceType: deviceType
        };
        $$(document).trigger('tokenInitiated');
        
        $$.ajax({
            url: options.api_url + "api/tokenAdd",
            type: "POST",
            data:"registrationId=" + registrationId + "&deviceUuId=" + deviceUuId + "&deviceType=" + deviceType
        });
    });

    push.on('notification', function(data) {
        console.log(data.additionalData);
        if(data.additionalData.foreground !== false) 
            return;

        var id = data.additionalData["id"],
            type = data.additionalData["type"];

        switch(type) {
            case "legislator":
                singleLegislator.loadPage(id);
                break;
            case "committee":
                singleCommittee.loadPage(id);
                break;
            case "bill":
                singleBill.loadPage(id);
                break;
            case "vote":
                singleVote.loadPage(id);
                break;
        }
        console.log("loaded");
    });

    push.on('error', function(e) {
        console.log("push error");
    });
}