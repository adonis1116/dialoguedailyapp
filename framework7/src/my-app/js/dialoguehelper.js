
// Dialogue Helper
var DialogueHelper = function(){
    //property
    //methods

    this.init = init;
    this.loadPage = loadPage;
    function init(){
    	$$('.dialogue-helper-page .icon-only.back').css('display', 'none');
        initEventListeners();
    }

    function initEventListeners(){
    	$$('.dialogue-helper-page .icon-only.back').on('click', function(){
    		$$('.dialogue-helper-page .icon-only.back').css('display', 'none');
        	$$('.dialogue-helper-page a.open-panel').css('display', 'flex');
    	});

        $$('.dialogue-helper-page .btn-ref').on('click', function(){
            var id = parseInt($$(this).data('id'));
            var pagename = '';
            if (id == 1) pagename = 'how-to';
            else if (id == 2) pagename = 'glossary';
            else if (id == 3) pagename = 'PIM';
            else if (id == 4) pagename = 'procedure';
            else if (id == 5) pagename = 'ground-rules';
            mainView.router.load({ pageName: pagename});
        });

        $$('.how-to-page .btn-ref').on('click', function(){
            var pagename = 'PIM';
            mainView.router.load({ pageName: pagename});
        });

        myApp.onPageBeforeInit('dialogue-helper', function(page){
            dbMgr.sendRequest('get_videos', {}, function(result){
                if (result['success']){
                    var video1 = result['data']['video1'];
                    var video2 = result['data']['video2'];
                    $$('.dialogue-helper-page .video-frame.correctly').html('<iframe src="' + video1 + '" frameborder="0" allowfullscreen></iframe>');
                    $$('.dialogue-helper-page .video-frame.not-dialogue').html('<iframe src="' + video2 + '" frameborder="0" allowfullscreen></iframe>');
                }
            });
        });
    }

    function loadPage(){
        mainView.router.load({pageName: 'dialogue-helper'});
    }


}

var helperMgr = new DialogueHelper();
helperMgr.init();