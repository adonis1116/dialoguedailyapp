
var CreateQuestion = function(){

    //property
    var questions = [],
        currentStep = 1,
        categoryId = -1,
        questionId = -1;
    
    var dateDOM;
    var writingDOM;
    var sharingDOM;
    var selectedDayofWeek;
    var selectedDate;
    var writingTime;
    var sharingTime;
    var dateSelected; // Special
    this.perPage = 20;
    this.page = 1;
    this.totalCount = 0;
    this.objects = [];
    this.listBlock= {};
    this.query = {};

    //method
    this.init = init;
    this.loadPage = loadPage;
    this.loadQuestionsList = loadQuestionsList;

    //method for infinite loading
    this.isEmpty = isEmpty;
    this.isFull = isFull;
    this.append = append;
    this.load = load;

    function init(){        
        currentStep = 1;
        categoryId = -1;
        initEventListeners();
        initDOMElements();
    }

    function loadPage(){
        
        currentStep = 1;
        this.page = 1;
        questionId = -1;
        categoryId = -1;
        showSteps(currentStep);

        showCategoriesList();
        $$('#create-question-step3.tab #calendar-appointment').css('visibility','hidden');
        $$('#create-question-step3.tab #writing-appointment').css('visibility','hidden');
        $$('#create-question-step3.tab #sharing-appointment').css('visibility','hidden');
        
        var today = new Date();
        /*var tomorrow = ( today.getHours() < 12 ? today :  new Date((new Date()).getTime() + 24 * 60 * 60 * 1000) );*/
        var tomorrow = today;
        dateSelected = new Date(tomorrow);

        selectedDayofWeek = tomorrow.getDay();
        var strDay;
        switch(selectedDayofWeek){
            case 1:
                strDay = "Monday";
            break;
            case 2:
                strDay = "Tuesday";
            break;
            case 3:
                strDay = "Wednesday";
            break;
            case 4:
                strDay = "Thursday";
            break;
            case 5:
                strDay = "Friday";
            break;
            case 6:
                strDay = "Saturday";
            break;
            case 0:
                strDay = "Sunday";
            break;
        }
        $$('.create-question-page #create-question-step3.tab .set-day').html(strDay);

        $$('.create-question-page #create-question-step3.tab .set-date').html(
            tomorrow.getFullYear() + '-' + staticMgr.changeNumberFormat(tomorrow.getMonth() + 1) + '-' + staticMgr.changeNumberFormat(tomorrow.getDate())
        );
        selectedDate = tomorrow.getFullYear() + '-' + staticMgr.changeNumberFormat(tomorrow.getMonth() + 1) + '-' + staticMgr.changeNumberFormat(tomorrow.getDate()); 
        $$('.create-question-page .btn-next-step').html('Next');
        $$('.create-question-page .center.sliding').html('Choose Dialogue Category');

        mainView.router.load({pageName: 'create-question'});
    }

    function initDOMElements(){
        showSteps(currentStep);
        // Set the height of Categories List
        var categoriesListHeight = staticMgr.screenHeight - staticMgr.navbarHeight - 44 - 32 * 2 - 100;
        $$('.create-question-page .categories-list').css('height', categoriesListHeight + 'px');
        $$('.create-question-page .questions-list').css('height', categoriesListHeight + 'px');
    }

    function initEventListeners(){

        dateDOM = myApp.calendar({
            input: '#calendar-appointment',
            disabled:{
                /*to: ( (new Date()).getHours() < 12 ? new Date((new Date()).getTime() - 24 * 60 * 60 * 1000) : new Date())*/
                to: new Date((new Date()).getTime() - 24 * 60 * 60 * 1000)
            },
            firstDay: 0,
            dateFormat: 'DD, MM dd, yyyy',
            onChange: function(p, value){
                var d = new Date(value);
                dateSelected = new Date(value);
                $$('.create-question-page #create-question-step3.tab .set-date').html(
                        d.getFullYear() + '-' + staticMgr.changeNumberFormat(d.getMonth() + 1) + '-' + staticMgr.changeNumberFormat(d.getDate())
                    );
                selectedDate = d.getFullYear() + '-' + staticMgr.changeNumberFormat(d.getMonth() + 1) + '-' + staticMgr.changeNumberFormat(d.getDate()); 
                var strDay;
                switch(d.getDay()){
                    case 1:
                        strDay = "Monday";
                    break;
                    case 2:
                        strDay = "Tuesday";
                    break;
                    case 3:
                        strDay = "Wednesday";
                    break;
                    case 4:
                        strDay = "Thursday";
                    break;
                    case 5:
                        strDay = "Friday";
                    break;
                    case 6:
                        strDay = "Saturday";
                    break;
                    case 0:
                        strDay = "Sunday";
                    break;
                }

                selectedDayofWeek = d.getDay();
                $$('.create-question-page #create-question-step3.tab .set-day').html(strDay);
                layoutLastStep();
            }
        });    
        initTimeModifyDOM();
        $$('.create-question-page .btn-prev-step').on('click', function(e){
            currentStep --;
            $$('.create-question-page .btn-next-step').addClass('color-blue');
            $$('.create-question-page .btn-next-step').removeClass('color-gray');
            $$('.create-question-page .btn-next-step').html('Next');
            if (currentStep <= 1){
                $$(this).removeClass('color-blue');
                $$(this).addClass('color-gray');
                currentStep = 1;
                $$('.create-question-page .center.sliding').html('Choose Dialogue Category');
            } 
            showSteps(currentStep);
        });        

        $$(document).on('click', '.create-question-page .btn-next-step', function(){
            if (currentStep == 1 && categoryId == -1){
                myApp.alert("Please choose a category.");
                return;
            }
            if (currentStep == 2 && questionId == -1){
                myApp.alert("Please choose a question.");
                return;
            }

            currentStep ++;
            $$('.create-question-page .btn-prev-step').addClass('color-blue');
            $$('.create-question-page .btn-prev-step').removeClass('color-gray');

            if (currentStep == 2){
                this.page = 1;
                QuestionInfiniteList($$('.create-question-page #create-question-step2'));
                $$('.create-question-page .center.sliding').html('Choose Dialogue Question');
            }

            if (currentStep >= 3){
                if ( currentStep > 3 ){
                    currentStep = 3;
                    createNextAppointment();
                }
                else{
                    currentStep = 3;
                    $$(this).html('Finish');
                    layoutLastStep();    
                }
            }

            showSteps(currentStep);
        });

        $$(document).on('click', '.create-question-page .categories-list ul li', function(){
            categoryId = parseInt($$(this).data('id'));
            questionId = -1;
            this.page = 1;
        });

        $$(document).on('click', '.create-question-page .questions-list ul li', function(){
            questionId = parseInt($$(this).data('id'));
            myApp.alert($$(this).find('label .item-inner .item-title').html(), 'Dialogue Question');
        });

        $$(document).on('click', '.create-question-page #create-question-step3.tab .item-date', function(){
            dateDOM.open();      
        });

        $$(document).on('click', '.create-question-page #create-question-step3.tab .item-writing', function(){
            writingDOM.open();  
        });

        $$(document).on('click', '.create-question-page #create-question-step3.tab .item-sharing', function(){
            sharingDOM.open();
        });
    }

    function initTimeModifyDOM(){
        var strHourValue = "", strMinuteValue = "";
        for ( var i = 1; i <= 12; i++)
        {
            if (i != 12) strHourValue += staticMgr.changeNumberFormat(i) + ' ';
            else if (i == 12)strHourValue += staticMgr.changeNumberFormat(i);
        }
        for ( var i = 0; i <= 59; i++)
        {
            if (i != 59) strMinuteValue += staticMgr.changeNumberFormat(i) + ' ';
            else if (i == 59)strMinuteValue += staticMgr.changeNumberFormat(i);
        }
        var newCols = [];
        newCols.push({
            textAlign: 'left',
            values: strHourValue.split(' ')
        });

        newCols.push({
            values: strMinuteValue.split(' ')
        });

        newCols.push({
            textAlign: 'right',
            values: ['AM', 'PM']
        });
        
        writingDOM = myApp.picker({
            input: '#writing-appointment',
            rotateEffect: true,
            closeByOutsideClick: true,
            cols: newCols,
            onOpen: function(p){
                var adInfo = user.appointmentDefaults; // AppointmentDefaults Info
                var arrayWritingTime;
                switch(selectedDayofWeek){
                    case 1:
                        arrayWritingTime = adInfo['mon_writing'].split(" ");
                    break;
                    case 2:
                        arrayWritingTime = adInfo['tue_writing'].split(" ");
                    break;
                    case 3:
                        arrayWritingTime = adInfo['wed_writing'].split(" ");
                    break;
                    case 4:
                        arrayWritingTime = adInfo['thu_writing'].split(" ");
                    break;
                    case 5:
                        arrayWritingTime = adInfo['fri_writing'].split(" ");
                    break;
                    case 6:
                        arrayWritingTime = adInfo['sat_writing'].split(" ");
                    break;
                    case 0:
                        arrayWritingTime = adInfo['sun_writing'].split(" ");
                    break;
                }
                writingTime = arrayWritingTime[0] + ' ' + arrayWritingTime[1] + ' ' + arrayWritingTime[2];
                var h = parseInt(arrayWritingTime[0]);
                var m = parseInt(arrayWritingTime[1]);
                var n = arrayWritingTime[2];
                p.setValue([ 
                            staticMgr.changeNumberFormat(h),
                            staticMgr.changeNumberFormat(m),
                            n ]);
            },
            onChange: function(p, values, displayValues){
                var h = parseInt(values[0]);
                var m = parseInt(values[1]);
                var n = values[2];
                var strWritingTime = staticMgr.changeNumberFormat(h) + ':' + staticMgr.changeNumberFormat(m) + ' ' + n;
                writingTime = values[0] + ' ' + values[1] + ' ' + values[2];
                $$('.create-question-page #create-question-step3.tab #writing-time').html(strWritingTime);    
                
            }
        });

        sharingDOM = myApp.picker({
            input: '#sharing-appointment',
            rotateEffect: true,
            closeByOutsideClick: true,
            cols: newCols,
            onOpen: function(p){
                var adInfo = user.appointmentDefaults; // AppointmentDefaults Info
                var arraySharingTime;
                switch(selectedDayofWeek){
                    case 1:
                        arraySharingTime = adInfo['mon_sharing'].split(" ");
                    break;
                    case 2:
                        arraySharingTime = adInfo['tue_sharing'].split(" ");
                    break;
                    case 3:
                        arraySharingTime = adInfo['wed_sharing'].split(" ");
                    break;
                    case 4:
                        arraySharingTime = adInfo['thu_sharing'].split(" ");
                    break;
                    case 5:
                        arraySharingTime = adInfo['fri_sharing'].split(" ");
                    break;
                    case 6:
                        arraySharingTime = adInfo['sat_sharing'].split(" ");
                    break;
                    case 0:
                        arraySharingTime = adInfo['sun_sharing'].split(" ");
                    break;
                }
                
                sharingTime = arraySharingTime[0] + ' ' + arraySharingTime[1] + ' ' + arraySharingTime[2];
                var h = parseInt(arraySharingTime[0]);
                var m = parseInt(arraySharingTime[1]);
                var n = arraySharingTime[2];
                p.setValue([ 
                            staticMgr.changeNumberFormat(h),
                            staticMgr.changeNumberFormat(m),
                            n ]);
            },
            onChange: function(p, values, displayValues){
                var h = parseInt(values[0]);
                var m = parseInt(values[1]);
                var n = values[2];
                var strSharingTime = staticMgr.changeNumberFormat(h) + ':' + staticMgr.changeNumberFormat(m) + ' ' + n;
                sharingTime = values[0] + ' ' + values[1] + ' ' + values[2];
                $$('.create-question-page #create-question-step3.tab #sharing-time').html(strSharingTime);
            }
        });
    }

    function createNextAppointment(){
        
        var nextAppointment = {
            questionid : questionId,
            date: selectedDate,
            writing_time: writingTime,
            sharing_time: sharingTime,
        };
        
        myApp.confirm('Send the Dialogue Question and Sharing Appointment to your spouse?', 'Confirm Dialogue Daily', function () {
            dbMgr.sendRequest('create_next_appointment', nextAppointment, function(result){
                if (result['success']){
                    user.nextAppointment = JSON.parse(JSON.stringify(result['data']));
                    myApp.alert(result['msg'], staticMgr.appString());
                    mainView.router.back();
                    scheduleAppointment();
                }
                else{
                    myApp.alert(result['msg'], staticMgr.appString());
                }
            });
        });
    }

    function scheduleAppointment(){

        cordova.plugins.notification.local.clearAll();

        var writingInfoList = writingTime.split(" "), sharingInfoList = sharingTime.split(" ");

        if ( writingInfoList[2] == "PM" ) dateSelected.setHours( parseInt(writingInfoList[0]) + 12 );
        else dateSelected.setHours( parseInt(writingInfoList[0]) );

        dateSelected.setMinutes( parseInt(writingInfoList[1]) );

        cordova.plugins.notification.local.schedule({
            text: "It's writing time. Please write a letter to send your spouse...",
            date: dateSelected
        });

        if ( sharingInfoList[2] == "PM" ) dateSelected.setHours( parseInt(sharingInfoList[0]) + 12 );
        else dateSelected.setHours( parseInt(sharingInfoList[0]) );

        dateSelected.setMinutes( parseInt(sharingInfoList[1]) );

        cordova.plugins.notification.local.schedule({
            text: "It's sharing time. Please share the letter with your spouse...",
            date: dateSelected
        });
    }

    function showSteps(stepNo){

        myApp.showTab('#create-question-step' + stepNo);
        
        $$('.create-question-page .steps-block .steps-chips .chip').removeClass('selected');
        
        for (var i = 1; i <= stepNo; i++){
            $$('.create-question-page .steps-block .steps-chips .chip.chip-step' + i + ' .chip-media').removeClass('bg-gray');
            $$('.create-question-page .steps-block .steps-chips .chip.chip-step' + i + ' .chip-media').addClass('bg-blue');
        }
        
        for (var i = stepNo + 1; i <= 3; i++){
            $$('.create-question-page .steps-block .steps-chips .chip.chip-step' + i + ' .chip-media').removeClass('bg-blue');
            $$('.create-question-page .steps-block .steps-chips .chip.chip-step' + i + ' .chip-media').addClass('bg-gray');
        }
        
        $$('.create-question-page .steps-block .steps-chips .chip.chip-step' + stepNo).addClass('selected');
    }

    function showCategoriesList(){
        $$('.create-question-page .categories-list ul li').remove();
        for(var i = 0 ; i < staticMgr.question_categories.length; i++){
            var categoryItem = '<li data-id="' + staticMgr.question_categories[i]['id'] +  '">' + 
                                    '<label class="label-radio item-content">' +
                                        '<input type="radio" name="my-radio" value="' + staticMgr.question_categories[i]['id'] +'">' +
                                        '<div class="item-media"><i class="icon icon-form-radio"></i></div>' +
                                        '<div class="item-inner">' +
                                            '<div class="item-title">' + staticMgr.question_categories[i]['category_content'] + '</div>' +
                                        '</div>' +
                                    '</label>' +
                                '</li>';
            $$('.create-question-page .categories-list ul').append(categoryItem);
        }

        // Set the height of Categories List
        var categoriesListHeight = staticMgr.screenHeight - staticMgr.navbarHeight - 44 - 32 * 2 - 100;
        $$('.create-question-page .categories-list').css('height', categoriesListHeight + 'px');
        
    }

    function layoutLastStep(){
        if (currentStep < 3) return;
        
        var adInfo = user.appointmentDefaults; // AppointmentDefaults Info
        var strWritingTime, strSharingTime, arrayWritingTime, arraySharingTime;
        switch(selectedDayofWeek){
            case 1:
                arrayWritingTime = adInfo['mon_writing'].split(" ");
                arraySharingTime = adInfo['mon_sharing'].split(" ");
            break;
            case 2:
                arrayWritingTime = adInfo['tue_writing'].split(" ");
                arraySharingTime = adInfo['tue_sharing'].split(" ");
            break;
            case 3:
                arrayWritingTime = adInfo['wed_writing'].split(" ");
                arraySharingTime = adInfo['wed_sharing'].split(" ");
            break;
            case 4:
                arrayWritingTime = adInfo['thu_writing'].split(" ");
                arraySharingTime = adInfo['thu_sharing'].split(" ");
            break;
            case 5:
                arrayWritingTime = adInfo['fri_writing'].split(" ");
                arraySharingTime = adInfo['fri_sharing'].split(" ");
            break;
            case 6:
                arrayWritingTime = adInfo['sat_writing'].split(" ");
                arraySharingTime = adInfo['sat_sharing'].split(" ");
            break;
            case 0:
                arrayWritingTime = adInfo['sun_writing'].split(" ");
                arraySharingTime = adInfo['sun_sharing'].split(" ");
            break;
        }
        
        strWritingTime = staticMgr.changeNumberFormat(arrayWritingTime[0]) + ':' + staticMgr.changeNumberFormat(arrayWritingTime[1]) + ' ' + arrayWritingTime[2];
        strSharingTime = staticMgr.changeNumberFormat(arraySharingTime[0]) + ':' + staticMgr.changeNumberFormat(arraySharingTime[1]) + ' ' + arraySharingTime[2];
        writingTime = arrayWritingTime[0] + ' ' + arrayWritingTime[1] + ' ' + arrayWritingTime[2];
        sharingTime = arraySharingTime[0] + ' ' + arraySharingTime[1] + ' ' + arraySharingTime[2];
        $$('.create-question-page #create-question-step3.tab #writing-time').html(strWritingTime);
        $$('.create-question-page #create-question-step3.tab #sharing-time').html(strSharingTime);
    }

    // Methods for Questions Infinite Loading
    function isEmpty() {
        return (this.objects.length == 0);
    }

    function isFull() {
        return (this.page * this.perPage >= this.totalCount);
    }

    function append(questions) {
        var list = $$(this.listBlock).find("ul");
        questions.map(function(question){
            list.append('<li data-id="' + question['id'] +  '">' + 
                                            '<label class="label-radio item-content">' +
                                                '<input type="radio" name="my-radio" value="' + question['id'] + '">' +
                                                '<div class="item-media"><i class="icon icon-form-radio"></i></div>' +
                                                '<div class="item-inner">' +
                                                    '<div class="item-title">' + question['content'] + '</div>' +
                                                '</div>' +
                                            '</label>' +
                                        '</li>');
        });
    }

    function loadQuestionsList(listBlock) {

        var categoriesListHeight = staticMgr.screenHeight - staticMgr.navbarHeight - 44 - 32 * 2 - 100;
        $$('.create-question-page .questions-list').css('height', categoriesListHeight + 'px');
        this.page = 1;
        var t = this;
        t.listBlock = listBlock;
        t.query.page = t.page;
        t.query.perPage = t.perPage;
        t.query.category_id = categoryId;

        dbMgr.sendRequest('questions', t.query, function(result){
            if( result['data'] == undefined || result['data'] == null || result['data'].length == 0){
                currentStep --;
                $$('.create-question-page .btn-next-step').addClass('color-blue');
                $$('.create-question-page .btn-next-step').removeClass('color-gray');
                $$('.create-question-page .btn-next-step').html('Next');
                if (currentStep <= 1){
                    $$(this).removeClass('color-blue');
                    $$(this).addClass('color-gray');
                    currentStep = 1;
                } 
                showSteps(currentStep);
                myApp.alert('No questions yet in this category.', 'Quetions');
                return;
            }

            t.objects = result['data'];
            t.totalCount = result['count'];
            $$('.create-question-page .questions-list ul').remove();
            questions = [];

            if(t.totalCount == 0) {
//                $$(t.listBlock).append("<div class='content-block'>Not found</div>");
            } else {
                $$(t.listBlock).append("<ul></ul>");
                t.append(t.objects);
            }
            $$(t.listBlock).trigger("loaded");
        });

    }

    function load() {
        var t = this;
        t.page ++;
        t.query.page = t.page;
        t.query.category_id = categoryId;
        dbMgr.sendRequest('questions', t.query, function(result){
            t.objects = result['data'];
            t.totalCount = result['count'];
            if(t.totalCount == 0) {
//                $$(t.listBlock).append("<div class='content-block'>Not found</div>");
            } else {
                $$(t.listBlock).append("<ul></ul>");
                t.append(t.objects);
            }
            $$(t.listBlock).trigger("loaded");
        });
    }
};

var QuestionInfiniteList = function(container) {
    // property
    this.container = container;

    // method
    this.init = init;

    function init() {
        var t = this,
            listBlock = t.container.find(".list-block");
            
        myApp.attachInfiniteScroll(container);

        listBlock.html("");
        createQuestionMgr.loadQuestionsList(listBlock);
        // Infinite Scrolling
        var loading = false;
        t.container.on('infinite', function (event) {
            if (loading || createQuestionMgr.isEmpty()) return;
            loading = true;
            createQuestionMgr.load();
        });
        listBlock.on("loaded", function(){
            loading = false;
            if(createQuestionMgr.isFull()) {
                myApp.detachInfiniteScroll(t.container);
                t.container.find('.infinite-scroll-preloader').remove();
                return;
            }
        });
        listBlock.on("failed", function(){
            loading = false;
            myApp.detachInfiniteScroll(t.container);
            t.container.find('.infinite-scroll-preloader').remove();
            return;
        });
    }
    this.init();
}