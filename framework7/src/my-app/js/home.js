var createQuestionMgr = new CreateQuestion();
createQuestionMgr.init();

var writeLetterMgr = new WriteLetter();
writeLetterMgr.init();

var shareLetterMgr = new ShareLetter();
shareLetterMgr.init();

var Home = function(){
    
    var isRefreshing = false;
    var refreshingTimer;
    //method
    this.init = init;
    this.loadPage = loadPage;
    this.gotoCreateQuestion = gotoCreateQuestion;
    this.gotoViewQuestion = gotoViewQuestion;
    this.gotoShareLetter = gotoShareLetter;

    function init(){
        initEventListeners();
    }

    function initEventListeners(){
        $$('.home-page .btn-view-question').on('click', function(){
            var today = new Date();
            var strADate = (user.nextAppointment['date']).split('-');
            var apDate = new Date(strADate[0],strADate[1] - 1,strADate[2]);
            if (sameDay(today, apDate)) homeMgr.gotoViewQuestion();
            else{
                if ( apDate  > today  ){
                    myApp.alert('Please wait until scheduled date to write.', 'Dialogue Daily');
                    return;
                }
                else if ( apDate  < today  ){
                    homeMgr.gotoViewQuestion();
/*                    myApp.confirm('You can not write for current appointment. Do you want to clear the appointment and create new appointment?', function(){
                        if ( user.nextAppointment['id'] != null || user.nextAppointment['id'] != undefined ){
                            dbMgr.sendRequest( 'clear_next_appointment', { id:user.nextAppointment['id'], reset: 0 }, function(result){
                                if (result['success']){
                                    user.nextAppointment = {};
                                    homeMgr.gotoCreateQuestion();
                                } 
                                else{
                                    myApp.alert('You can not create new appointment. Please contact to Administrator.', 'Dialogue Daily');
                                }
                            });
                        }    
                    });
                    return;*/
                }
            }
        });
        
        $$('.home-page .btn-create-question').on('click', function(){
            homeMgr.gotoCreateQuestion();
        });

        $$('.home-page .btn-share-letter').on('click', function(){
            homeMgr.gotoShareLetter();
        });

        $$('.home-page .btn-reset-appointment').on('click', function(){
            myApp.confirm('Are you sure you want to select a new question?', function () {
                resetAppointment();
            });
        });

        $$('.btn-home').on('click', function(){
            mainView.router.load({pageName:'home'});
        });
        
        $$('.home-page .btn-home').on('click', function(){
            homeMgr.loadPage();
        });

        $$('.home-page .pull-to-refresh-content').on('ptr:refresh', function (e) {
            if (isRefreshing) return;
            isRefreshing = true;
            refreshingTimer = window.setInterval(function(){
                if (!isRefreshing){
                    window.clearInterval(refreshingTimer);
                    myApp.pullToRefreshDone();
                } 
            }, 100);
            homeMgr.loadPage();
        });

        $$(document).on("click", ".clear-link", function() {
            myApp.confirm('Are you sure you want to clear the current question?', function () {
                resetAppointment();
            });
        });
    }

    function checkLetterInfo(callback){
        
        if ( user.letterInfo['userid'] == null || user.letterInfo['userid'] == undefined ){
            dbMgr.sendRequest('get_letter', {}, function(result){
                if (result['success']){
                    user.letterInfo = JSON.parse(JSON.stringify(result['data']));
                    if (user.letterInfo['isshared'] == '0')
                        callback(true, false);
                    else
                        callback(true, true);
                }
                else{
                    callback(false, false);
                }
            });
        }
        else{
            if (user.letterInfo['isshared'] == '0')
                callback(true, false);
            else
                callback(true, true);
        }
    }

    function noQuestion(){
        $$('.home-page .btn-view-question').css('display', 'none');
        $$('.home-page .btn-reset-appointment').css('display', 'none');
        $$('.home-page .btn-home').css('display', 'flex');
        $$('.home-page .btn-create-question').css('display', 'block');    
        $$('.home-page .btn-create-question').css('visibility', 'visible');  
        $$('.home-page .btn-share-letter').css('display', 'none');
    }

    function hasQuestion(){
        $$('.home-page .btn-reset-appointment').css('display', 'none');
        $$('.home-page .btn-home').css('display', 'flex');
        $$('.home-page .btn-view-question').css('display', 'block');
        $$('.home-page .btn-create-question').css('display', 'none');
    }

    function hasLetter(){
        $$('.home-page .btn-view-question').css('display', 'none');
        $$('.home-page .btn-create-question').css('display', 'none');
        $$('.home-page .btn-share-letter').css('display', 'block');
        $$('.home-page .btn-reset-appointment').css('display', 'none');
        $$('.home-page .btn-home').css('display', 'flex');
    }

    function sharedLetter(){
        $$('.home-page .btn-view-question').css('display', 'none');
        $$('.home-page .btn-create-question').css('display', 'none');
        $$('.home-page .btn-reset-appointment').css('display', 'none');
        $$('.home-page .btn-home').css('display', 'flex');
        $$('.home-page .btn-share-letter').css('display', 'block');
        $$('.home-page .btn-share-letter').prop('enabled', false);
    }

    function loadPage(){
        $$('.list-block.menu-list a.clear-link').css('display', 'block');
        checkLetterInfo(function(has, shared){
            dbMgr.sendRequest( 'get_next_appointment', {}, function(result){
                if (result['success']){
                    hasQuestion();
                    user.nextAppointment = JSON.parse(JSON.stringify(result['data']));
                    if (user.nextAppointment['iswriting'] == 1){
                        $$('.list-block.menu-list a.clear-link').css('display', 'none');    
                    }
                    layoutNextAppointment(true);
                    if (has){
                        if (!shared)
                            hasLetter();
                        else
                            sharedLetter();
                    }
                }
                else{
                    noQuestion();
                    layoutNextAppointment(false);
                    $$('.list-block.menu-list a.clear-link').css('display', 'none');
                }
            });
        });
    }

    function loadDefaultsData(){
        if ( user.timerInfo['id'] == undefined || user.timerInfo['id'] == null )
        {
            dbMgr.sendRequest( 'get_timer', {} , function(result){
                if ( result['success'] ){
                    user.timerInfo = JSON.parse(JSON.stringify(result['data']));
                }
                else{
                    myApp.alert('Failed to get timer defaults.' , 'Timer Defaults');
                }
            });
        }
        if ( user.emailWords['id'] == null || user.emailWords['id'] == undefined ){
            dbMgr.sendRequest('get_email_wordings', {}, function(result){
                if ( result['success'] ){
                    var data = result['data'];
                    user.emailWords = data;
                }
                else{
                    myApp.alert('Failed to get Email Wordings.' , 'Email Wordings');
                }
            });
        }
    }

    function gotoViewQuestion(){
        if ( user.timerInfo['id'] == undefined || user.timerInfo['id'] == null ){
            myApp.alert('Please checkout Timer Defaults in Settings Menu.' , 'Dialogue Daily');
        }
        else{
            
            var today = new Date();
            var temp = user.getWritingFromLocal();

            if (!temp){
                user.clearWriting();
                dbMgr.sendRequest( 'update_appointment', {id: user.nextAppointment['id']}, function(result){
                    if (result['success']){
                        user.nextAppointment['iswriting'] = 1;
                        writeLetterMgr.loadPage();
                    }
                    else{
                        myApp.alert(result['msg'], staticMgr.appString());
                    }
                });
            }
            else {
                var writingContents = JSON.parse(JSON.stringify(temp));
                
                var savedDate = new Date(writingContents["date"]);
                if ( today.getFullYear() == savedDate.getFullYear() && today.getDay() == savedDate.getDay() && today.getMonth() == savedDate.getMonth()){
                    writeLetterMgr.continueWriting(writingContents);
                }
                else{
                    user.clearWriting();
                    writeLetterMgr.loadPage();
                }                
            }
        }
    }

    function gotoCreateQuestion(){
        if ( user.appointmentDefaults['id'] == null || user.appointmentDefaults['id'] == undefined ){
            myApp.alert('Please checkout Appointment Defaults in Settings Menu.' , 'Dialogue Daily');
        }
        else {
            createQuestionMgr.loadPage();    
        }
    }

    function gotoShareLetter(){
        shareLetterMgr.loadPage();
    }

    function layoutNextAppointment(has){
        var strDate;
        var strWritingTime;
        var strSharingTime;
        var strQuestion;
        if (has){
            strDate = user.nextAppointment['date'];
            var arrayWritingTime = (user.nextAppointment['writing_time']).split(' ');
            var arraySharingTime = (user.nextAppointment['sharing_time']).split(' ');
            strWritingTime = staticMgr.changeNumberFormat(arrayWritingTime[0]) + ':' + staticMgr.changeNumberFormat(arrayWritingTime[1]) + ' ' + arrayWritingTime[2] ;
            strSharingTime = staticMgr.changeNumberFormat(arraySharingTime[0]) + ':' + staticMgr.changeNumberFormat(arraySharingTime[1]) + ' ' + arraySharingTime[2] ;

            dbMgr.sendRequest( 'get_question_by_id', {id: user.nextAppointment['questionid']}, function(result){
                if (result['data']['success']){
                    var data = JSON.parse(JSON.stringify(result['data']['data']));
                    strQuestion = data['content'];
                    $$('.home-page #next-question').html(strQuestion);
                    user.todayQuestion = strQuestion;
                }
                else{
                    myApp.alert(result['msg'], staticMgr.appString());
                }
                isRefreshing = false;
            });
            strQuestion = "";
        }
        else{
            strDate = 'None';
            strWritingTime = 'None';
            strSharingTime = 'None';
            strQuestion = 'No dialogue question';
            isRefreshing = false;
        }
        $$('.home-page #appointment-date').html(strDate);
        $$('.home-page #appointment-writing').html(strWritingTime);
        $$('.home-page #appointment-sharing').html(strSharingTime);    
        $$('.home-page #next-question').html(strQuestion);
        loadDefaultsData();
        mainView.router.load({pageName:'home'});
    }

    function resetAppointment(){
        if ( user.nextAppointment['id'] != null || user.nextAppointment['id'] != undefined ){
            // check if the next appointment from db is empty
            dbMgr.sendRequest( 'clear_next_appointment', { id:user.nextAppointment['id'], reset: 1 }, function(result){
                if (result['success']){
                    user.nextAppointment = {};
                    homeMgr.loadPage();
                } 
                myApp.alert(result['msg'], staticMgr.appString());
            });
        }
    }

    function sameDay(d1, d2){
        return d1.getFullYear() === d2.getFullYear()
            && d1.getDate() === d2.getDate()
            && d1.getMonth() === d2.getMonth();
    }
};

var homeMgr = new Home();
homeMgr.init();