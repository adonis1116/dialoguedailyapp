// Appointment Defaults Page
var AppointmentDefaults = function(){
    
    //property
    var daysOfWeek = ['S', 'M', 'T', 'W', 'T', 'F', 'S']; // 1: Sunday 2: Monday 3: Tuesday 4: Wednesday 5: Thursday 6: Friday 7: Saturday
    var selectedDay = 0; // non-selected // 1: Sunday 2: Monday 3: Tuesday 4: Wednesday 5: Thursday 6: Friday 7: Saturday

    var writingHour = 9;
    var writingMin = 0;
    var writingNoon = 'AM';
    var sharingHour = 6;
    var sharingMin = 0;
    var sharingNoon = 'PM';

    var tempDefaults;
    var selectedDays = [];
    //method
    this.init = init;
    this.loadPage = loadPage;

    // Initialize the class
    function init(){
        initEventListeners();
    }

    function initEventListeners(){
        $$('.appointment-defaults-page .week-chips .chip').on('click', function(e){

            if ($$(this).hasClass('selected')){
                $$(this).removeClass('selected');
                    
                var index = selectedDays.indexOf(parseInt($$(this).data('id')));
                if ( index > -1 ){
                    selectedDays.splice( index, 1);
                }
                if (selectedDays.length == 0) selectedDay = 0; // unselect
            }
            else{
                 // select one
                $$(this).addClass('selected');
                
                selectedDay = parseInt($$(this).data('id'));
                var writingTimeInfo, sharingTimeInfo;
                var tempUserDefaults = JSON.parse(JSON.stringify(tempDefaults));
                switch(selectedDay){
                    case 1:
                        writingTimeInfo = tempUserDefaults['mon_writing'].split(' ');
                        sharingTimeInfo = tempUserDefaults['mon_sharing'].split(' ');                      
                    break;
                    case 2:
                        writingTimeInfo = tempUserDefaults['tue_writing'].split(' ');
                        sharingTimeInfo = tempUserDefaults['tue_sharing'].split(' ');
                    break;
                    case 3:
                        writingTimeInfo = tempUserDefaults['wed_writing'].split(' ');
                        sharingTimeInfo = tempUserDefaults['wed_sharing'].split(' ');
                    break;
                    case 4:
                        writingTimeInfo = tempUserDefaults['thu_writing'].split(' ');
                        sharingTimeInfo = tempUserDefaults['thu_sharing'].split(' ');
                    break;
                    case 5:
                        writingTimeInfo = tempUserDefaults['fri_writing'].split(' ');
                        sharingTimeInfo = tempUserDefaults['fri_sharing'].split(' ');
                    break;
                    case 6:
                        writingTimeInfo = tempUserDefaults['sat_writing'].split(' ');
                        sharingTimeInfo = tempUserDefaults['sat_sharing'].split(' ');
                    break;
                    case 7:
                        writingTimeInfo = tempUserDefaults['sun_writing'].split(' ');
                        sharingTimeInfo = tempUserDefaults['sun_sharing'].split(' ');
                    break;
                }

                writingHour = parseInt( writingTimeInfo[0] );
                writingMin = parseInt( writingTimeInfo[1] );
                writingNoon = writingTimeInfo[2];
                sharingHour = parseInt( sharingTimeInfo[0] );
                sharingMin = parseInt( sharingTimeInfo[1] );
                sharingNoon = sharingTimeInfo[2];
                selectedDays.push(parseInt($$(this).data('id')));

                setWritingTime();
                setSharingTime();
            }
        });

        $$(document).on('click', '.appointment-defaults-page .clock-writing .clock-upper-section .btn-inc-hour', function(){
            if (selectedDay == 0){
                myApp.alert("Please select days to change time");
                return;
            }
            writingHour ++;
            if (writingHour > 12) writingHour = 1;
            setWritingTime();
        });

        $$(document).on('click', '.appointment-defaults-page .clock-writing .clock-upper-section .btn-inc-min', function(){
            if (selectedDay == 0){
                myApp.alert("Please select days to change time");
                return;
            }
            writingMin ++;
            if (writingMin > 59) writingMin = 0;
            setWritingTime();
        });

        $$(document).on('click', '.appointment-defaults-page .clock-writing .clock-upper-section .btn-inc-noon', function(){
            if (selectedDay == 0){
                myApp.alert("Please select days to change time");
                return;
            }
            setWritingNoon(writingNoon);
            setWritingTime();
        });

        $$(document).on('click', '.appointment-defaults-page .clock-writing .clock-bottom-section .btn-dec-hour', function(){
            if (selectedDay == 0){
                myApp.alert("Please select days to change time");
                return;
            }
            writingHour --;
            if (writingHour < 1) writingHour = 12;
            setWritingTime();
        });

        $$(document).on('click', '.appointment-defaults-page .clock-writing .clock-bottom-section .btn-dec-min', function(){
            if (selectedDay == 0){
                myApp.alert("Please select days to change time");
                return;
            }
            writingMin --;
            if (writingMin < 0) writingMin = 59;
            setWritingTime();
        });

        $$(document).on('click', '.appointment-defaults-page .clock-writing .clock-bottom-section .btn-dec-noon', function(){
            if (selectedDay == 0){
                myApp.alert("Please select days to change time");
                return;
            }
           setWritingNoon(writingNoon);
           setSharingTime();
        });

        $$(document).on('click', '.appointment-defaults-page .clock-sharing .clock-upper-section .btn-inc-hour', function(){
            if (selectedDay == 0){
                myApp.alert("Please select days to change time");
                return;
            }
            sharingHour ++;
            if (sharingHour > 12) sharingHour = 1;
            setSharingTime();
        });

        $$(document).on('click', '.appointment-defaults-page .clock-sharing .clock-upper-section .btn-inc-min', function(){
            if (selectedDay == 0){
                myApp.alert("Please select days to change time");
                return;
            }
            sharingMin ++;
            if (sharingMin > 59) sharingMin = 0;
            setSharingTime();
        });

        $$(document).on('click', '.appointment-defaults-page .clock-sharing .clock-upper-section .btn-inc-noon', function(){
            if (selectedDay == 0){
                myApp.alert("Please select days to change time");
                return;
            }
            setSharingNoon(sharingNoon);
            setSharingTime();
        });

        $$(document).on('click', '.appointment-defaults-page .clock-sharing .clock-bottom-section .btn-dec-hour', function(){
            if (selectedDay == 0){
                myApp.alert("Please select days to change time");
                return;
            }
            sharingHour --;
            if (sharingHour < 1) sharingHour = 12;
            setSharingTime();
        });

        $$(document).on('click', '.appointment-defaults-page .clock-sharing .clock-bottom-section .btn-dec-min', function(){
            if (selectedDay == 0){
                myApp.alert("Please select days to change time");
                return;
            }
            sharingMin --;
            if (sharingMin < 0) sharingMin = 59;
            setSharingTime();
        });

        $$(document).on('click', '.appointment-defaults-page .clock-sharing .clock-bottom-section .btn-dec-noon', function(){
            if (selectedDay == 0){
                myApp.alert("Please select days to change time");
                return;
            }
            setSharingNoon(sharingNoon);
            setSharingTime();
        });

        $$(document).on('click', '.appointment-defaults-page .navbar .save-appointment-defaults', function(){
            // Save to DB
            dbMgr.sendRequest('update_appointment_defaults', tempDefaults, function(result){
                if ( result['success'] ){
                    var data = result['data'];
                    user.appointmentDefaults = JSON.parse(JSON.stringify(tempDefaults));
                    myApp.alert(result['msg'], staticMgr.appString());
                    if (staticMgr.isFirstLoad) homeMgr.loadPage();
                    else mainView.router.back();
                }
                else{
                    myApp.alert('Failed to update appointment defaults.' , 'Timer Defaults');
                }
            });
        });
    }

    function loadPage(){
        loadDataFromDB();
        selectedDays = [];
        mainView.router.load({pageName: 'appointment-defaults'});
    }

    function loadDataFromDB(){

        for (var i = 1; i <= 7; i ++){
            $$('.appointment-defaults-page .week-chips .chip#chip' + i).removeClass('selected');
        }
        
        if ( user.appointmentDefaults['id'] == null || user.appointmentDefaults['id'] == undefined ){
            dbMgr.sendRequest('get_appointment_defaults', {}, function(result){
                if ( result['success'] ){
                    var data = result['data'];
                    user.appointmentDefaults = JSON.parse(JSON.stringify(data));
                    tempDefaults = JSON.parse(JSON.stringify(data));
                    layoutAppointmentDefaults();
                }
                else{
                    myApp.alert('Failed to get timer defaults.' , 'Appointment Defaults');
                }
            });
        }
        else {
            tempDefaults = JSON.parse(JSON.stringify( user.appointmentDefaults ));
            layoutAppointmentDefaults();
        }
    }

    function layoutAppointmentDefaults(){
        setWritingTime();
        setSharingTime();
    }

    function setWritingTime(){
        var strTime = writingHour + ' ' + writingMin + ' ' + writingNoon;
        for (var i = 0; i < selectedDays.length; i++){
            switch(selectedDays[i]){
                case 1:
                    tempDefaults['mon_writing'] = strTime;
                break;
                case 2:
                    tempDefaults['tue_writing'] = strTime;
                break;
                case 3:
                    tempDefaults['wed_writing'] = strTime;
                break;
                case 4:
                    tempDefaults['thu_writing'] = strTime;
                break;
                case 5:
                    tempDefaults['fri_writing'] = strTime;
                break;
                case 6:
                    tempDefaults['sat_writing'] = strTime;
                break;
                case 7:
                    tempDefaults['sun_writing'] = strTime;
                break;
            }
        }
    
        $$('.appointment-defaults-page .clock-writing .clock-current-section .current-min').html(staticMgr.changeNumberFormat(writingMin));
        $$('.appointment-defaults-page .clock-writing .clock-current-section .current-hour').html(staticMgr.changeNumberFormat(writingHour));
        $$('.appointment-defaults-page .clock-writing .clock-current-section .current-noon').html(writingNoon);
    }
    function setSharingTime(){
        var strTime = sharingHour + ' ' + sharingMin + ' ' + sharingNoon;
        for (var i = 0; i < selectedDays.length; i++){
            switch(selectedDays[i]){
                case 1:
                    tempDefaults['mon_sharing'] = strTime;
                break;
                case 2:
                    tempDefaults['tue_sharing'] = strTime;
                break;
                case 3:
                    tempDefaults['wed_sharing'] = strTime;
                break;
                case 4:
                    tempDefaults['thu_sharing'] = strTime;
                break;
                case 5:
                    tempDefaults['fri_sharing'] = strTime;
                break;
                case 6:
                    tempDefaults['sat_sharing'] = strTime;
                break;
                case 7:
                    tempDefaults['sun_sharing'] = strTime;
                break;
            }
        }

        $$('.appointment-defaults-page .clock-sharing .clock-current-section .current-min').html(staticMgr.changeNumberFormat(sharingMin));
        $$('.appointment-defaults-page .clock-sharing .clock-current-section .current-hour').html(staticMgr.changeNumberFormat(sharingHour));
        $$('.appointment-defaults-page .clock-sharing .clock-current-section .current-noon').html(sharingNoon);
    }
    function setWritingNoon(value){
        if ( value == 'AM' ){
            writingNoon = 'PM';
        }
        else {
            writingNoon = 'AM';
        }
    }
    function setSharingNoon(value){
        if ( value == 'AM' ){
            sharingNoon = 'PM';
        }
        else {
            sharingNoon = 'AM';
        }
    }
}