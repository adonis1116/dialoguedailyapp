    
var StaticManager = function(){

	//property
	this.screenHeight;
	this.navbarHeight;

	this.todayQuestion = "";

	this.question_categories = [];
	this.feeling_categories = [];
    this.questions = [];
    this.feelings = [];

	//method
	this.init = init;
	this.changeNumberFormat = changeNumberFormat;
    this.validateEmail = validateEmail;
    this.appString = appString;
    this.isFirstLoad = true;

	function init()
	{
		this.screenHeight = $$(window).height();
        this.navbarHeight = $$('.navbar').height();
        this.todayQuestion = "You haven't selected questions for today.";
	}

	function changeNumberFormat(number){
        var strNumber = number.toString();
        // change number format : 1 to 01
        if ( strNumber.length < 2 ) {
            strNumber = "0".concat(strNumber);
        }
        return strNumber;
    }  

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function appString(){
        return "Dialogue Daily";
    }
};
