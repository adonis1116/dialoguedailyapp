// include other classes
// Init Settings Page

// Create a Instance for Contact Info 
var contactInfoMgr = new ContactInfo();
contactInfoMgr.init();

// Create a Instance for Timer Defaults
var timerDefaultsMgr = new TimerDefaults();
timerDefaultsMgr.init();

// Create a Instance for Appointment Defaults
var appointmentDefaultsMgr = new AppointmentDefaults();
appointmentDefaultsMgr.init();

// Create a Instance for System Email Wordings
var systemEmailMgr = new SystemEmailWordings();
systemEmailMgr.init();

// Create a Instance for Share
var shareMgr = new Share();
shareMgr.init();

function initSettingsEventListeners(){
	// Event Listener for Go to Contact Info Page
	$$(document).on('click', '.item-contact-info', function(){
		contactInfoMgr.loadPage();
	});	

	// Event Listener for Go to Timer Defaults Page
	$$(document).on('click', '.item-timer-defaults', function(){
		timerDefaultsMgr.loadPage();
	});	
	
	// Event Listener for Go to Timer Defaults Page
	$$(document).on('click', '.item-appointment-defaults', function(){
		staticMgr.isFirstLoad = false;
		appointmentDefaultsMgr.loadPage();
	});	

	// Event Listener for Go to System Email Wordings Page
	$$(document).on('click', '.item-system-email', function(){
		systemEmailMgr.loadPage();
	});	

	// Event Listener for Go to About Us Page
	/*$$(document).on('click', '.item-about-us', function(){
		mainView.router.load({pageName:'about-us'});
	});	*/

	// Event Listener for Go to Share Page
	/*$$(document).on('click', '.item-share', function(){
		shareMgr.loadPage();
	});	*/
	
	$$('.settings-page').click('li.item-link.item-content.item-change-password', function(){
		mainView.router.load({pageName:'reset-password-screen'});
	});

	$$('.reset-password-page').click('.change-password', function(){

		var newPassword = $$('.reset-password-page #password').val();
		var confirmPassword = $$('.reset-password-page #confirm').val();

		if ( newPassword == '' || confirmPassword == '' ){
			myApp.alert( 'Please input correctly.', 'Dialogue Daily' );
			return;
		}

		if ( newPassword != confirmPassword ){
			myApp.alert( 'Please input correctly.', 'Dialogue Daily' );
			return;
		}

		myApp.confirm('Are you sure to set new password?', 'Change Password', function () {
	        dbMgr.sendRequest('resetpassword', { password: newPassword  }, function(result){
                if (result['success']) {
                    mainView.router.back();
                }
                myApp.alert( result['msg'], 'Dialogue Daily' );
            });
	    });
	});	
	
}

initSettingsEventListeners();