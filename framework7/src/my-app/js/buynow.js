var BuyNow = function(){

	this.init = init;

	function init(){
		initEventListeners();		
	}

	function initEventListeners(){
		$$('.buy-now-page .btn-upgrade').on('click', function(){
			var actionSheet;
			var payBtns;

			if (user.userInfo['level'] == 2){
				myApp.alert("You already upgraded your account to 'Lifetime'.", 'Dialogue Daily');
			}
			else{
				payBtns = [
			        {
			            text: 'Please upgrade your account by',
			            label: true,
			            bg: 'green'
			        },
			        {
			            text: 'Annual Pay',
			            bold: true,
			            onClick: function () {
			                myApp.confirm('Are you sure to purchase <Annual Plan> for $4.99 now? \n You can use this app for 1 year.', function () {
			                	upgradeAccount(1);
						    });
			            }
			        },
			        {
			            text: 'Lifetime Pay',
			            bold: true,
			            onClick: function () {
			                myApp.confirm('Are you sure to purchase <Lifetime Plan> for $19.99 now? \n You can use this app for your lifetime.', function () {
			                	upgradeAccount(2);
						    });
			            }
			        }
			    ];
			    myApp.actions(payBtns);
			}
		});
	}

	function upgradeAccount(mode){
		var productIds = ['com.awsns.dialoguedaily.yearly', 'com.awsns.dialoguedaily.lifetime'];
		var productId = productIds[ mode - 1 ];
		
		if ( mode == 1 ){
			inAppPurchase
				.buy(productId)
				.then(function (data) {
					saveToDB(mode, data.transactionId);
					return inAppPurchase.consume(data.productType, data.receipt, data.signature);
				})
				.then(function () {

				})
				.catch(function (err) {
					myApp.alert("Cancelled to upgrade your account." , "Dialogue Daily");
				});
		}
		else if ( mode == 2 ){
			inAppPurchase
				.buy(productId)
				.then(function (data) {
					saveToDB(mode, data.transactionId);
				})
				.catch(function (err) {
					myApp.alert("Cancelled to upgrade your account." , "Dialogue Daily");
				});
		}
	}

	function saveToDB(mode, transactionId){
		dbMgr.sendRequest('upgrade_account', {mode: mode, transactionId: transactionId}, function(result){
            if (result['success']){
            	user.userInfo = JSON.parse(JSON.stringify(result['data']));
    	        if ( user.userInfo['level'] == '2' ){
                    $$('.list-block.menu-list a#buy-now-page-link').css('display', 'none');
                }
            }
            myApp.alert(result['msg'], staticMgr.appString());
        });
	}
};

var buyMgr = new BuyNow();
buyMgr.init();