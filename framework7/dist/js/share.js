var Share = function(){

    //property

    //method
    this.init = init;
    this.loadPage = loadPage;
    this.shareApp = shareApp;

    function init(){
        initEventListeners();
    }

    function initEventListeners(){

        $$(document).on('click', '.share-page .btn-facebook', function(){
            myApp.alert('Facebook sharing');
        });

        $$(document).on('click', '.share-page .btn-google-plus', function(){
            myApp.alert('Google Plus sharing');
        });

        $$(document).on('click', '.share-page .btn-twitter', function(){
            myApp.alert('Twitter sharing');
        });

        $$(document).on('click', '.share-page .btn-linkedin', function(){
            myApp.alert('LinkedIn sharing');
        });

        $$(document).on('click', '.share-page .btn-instagram', function(){
            myApp.alert('Instagram sharing');
        });

        $$(document).on('click', '.share-page .btn-pinterest', function(){
            myApp.alert('Pinterest sharing');
        });
    
    }
    
    function loadPage(){
        mainView.router.load({pageName:'share'});
    }

    function shareApp(){

    }
};