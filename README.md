# Dialogue Daily

This is a project based on Cordova with Framework 7.

Prerequisites

> Node.js : v6.11.3

> Cordova : v7.0.1

Install dependencies for build:

> npm install -g grunt

> npm install -g gulp

------------- Build --------------------

In framework7 subdirectory
Install dependencies:

> npm install

To preview and watch for changes.
> gulp

Build for test.
> gulp build 

Build for production.
> gulp dist

Run --env production for use production endpoint

In root directory

Install dependencies:
> npm install

To move framework7 distribution to cordova project.
> grunt copy

Build for iOS

> cordova platform add ios

> cordova platform build ios

Build for Android

> cordova platform add android

> cordova platform build android

Or you can open iOS projects and Android Studio projects in platforms directory.

You need to remove -ObjC from your xCode project to build ios app: Build Settings > Other Linker Flags > remove -ObjC